package at.ji.wbchkr;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import at.ji.wbchkr.CheckSystem.WorkTask;
import at.ji.wbchkr.nn.Analizer;
import at.ji.wbchkr.nn.Analizer.Config;
import at.ji.wbchkr.nn.Analizer.Config.AnalizeMode;
import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class GlobalThread extends Thread
{
	private final CheckSystem[ ] systems;
	private final ControlFrame frame;
	private boolean working = true;
	private final Analizer analizer;
	private long lastTick;

	public GlobalThread(final CheckSystem[ ] systems)
	{
		super();
		this.systems = systems;
		frame = new ControlFrame(this);
		frame.setVisible(true);
		setName("Global thread");
		analizer = new Analizer(
				new File("agents/" + systems[ 0 ].getConfiguration().getString("nn.agentFile")),
				systems[ 0 ].getConfiguration().getInt("nn.minibtchSize"),
				"�������� ������");
	}

	public Object getLock()
	{
		return systems;
	}

	public CheckSystem[ ] getSystems()
	{
		return systems;
	}

	public void requestExit()
	{
		System.out.println("Requesting exit global thread");
		working = false;
	}

	@Override
	public void run()
	{
		if (systems.length == 0)
		{
			System.err.println("��� ������ ��� �������");
			return;
		}
		while (working)
		{
			synchronized (getLock())
			{

				final CheckSystem checkSystem = systems[ 0 ];
				final long t0 = System.currentTimeMillis();
				final ImageRegistration[ ] registrations = prepare();
				if (System.currentTimeMillis() - t0 < 10)
				{
					System.out.println("Global thread lag!");
				}
				else
				{
//					System.out.println("Prepring took " + (System.currentTimeMillis() - t0));
				}

				final long t0_ = System.currentTimeMillis();
				boolean detectDeffects = false;//TODO separate
				boolean detectProvyazka = false;
				for (final CheckSystem system : systems)
				{
					if (system.getWorkTask().equals(WorkTask.WORK))
					{
						detectDeffects = true;
						detectProvyazka = true;
					}
					else if (!system.getWorkTask().equals(WorkTask.NONE))
					{
						detectProvyazka = true;
					}
				}
				final AnalizeMode analizeMode = AnalizeMode.BY_MARK;
				final double settings_nn_statistics_dropout = 0;
				final double settings_nn_statistics_maxTopValueLevel = 0;
				final double settings_web_maxEdgeDifference = checkSystem
						.getConfiguration()
						.getDouble("nn.edgeFix.maxEdgeDifference");
				final double settings_edgefix_minValue = checkSystem
						.getConfiguration()
						.getDouble("nn.edgeFix.minValue");
				try
				{
					final Config config = Analizer.Config
							.create(
									settings_nn_statistics_dropout,
									settings_nn_statistics_maxTopValueLevel,
									detectDeffects,
									detectProvyazka,
									null,
									analizeMode,
									settings_edgefix_minValue / 255.0,
									1,
									settings_web_maxEdgeDifference / 255.0);
					analizer.analyze(registrations, config);
				}
				catch (final Exception e)
				{
					// TODO Auto-generated catch block
//					System.out.println(Arrays.toString(registrations));
					e.printStackTrace();
				}
//				System.out.println("Analizing took " + (System.currentTimeMillis() - t0_));

				updateSystems(registrations);
				if (systems[ 0 ].getConfiguration().getBoolean("debug", false))
				{
					System.out.println("Global thread tick");
				}
				lastTick = System.currentTimeMillis();
				for (final CheckSystem system : systems)
				{
					system.globalThreadTickNotify();
				}
				try
				{
					Thread.sleep(1);
				}
				catch (final Exception e)
				{
					throw new RuntimeException(e);
				}
			}
		}
		for (final CheckSystem system : systems)
		{
			try
			{
				system.saveConfig();
				system.deinitialize();
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
		System.out.println("Exit analization thread");
		System.exit(0);
	}

	public void updateAgents()
	{
		synchronized (getLock())
		{

		}
	}

	private ImageRegistration[ ] prepare()
	{
		final int minibatchSize = systems[ 0 ].getConfiguration().getInt("nn.minibatchSize");
		final Map<Integer, List<ImageRegistration>> map = new TreeMap<>();
		int size = 0;
		{
			while (size < minibatchSize)
			{
				ImageRegistration reg = null;
				for (int owner = 0; owner < systems.length; owner++)
				{
					final CheckSystem system = systems[ owner ];
					if (!map.containsKey(owner))
					{
						map.put(owner, new ArrayList<>());
					}

					if (system.has())
					{
						reg = system.poll();
						reg.owner = owner;
						if (reg.data == null)
						{
							System.err.println("Null reg.data");
							reg = null;
							continue;
						}
						map.get(owner).add(reg);
						size++;
						continue;
					}
				}
				try
				{
					Thread.sleep(1);
				}
				catch (final Exception e)
				{
					throw new RuntimeException(e);
				}
			}
			final List<ImageRegistration> list = new ArrayList<>();
			final Collection<List<ImageRegistration>> set = map.values();
			for (final List<ImageRegistration> listMapped : set)
			{
				list.addAll(listMapped);
			}
			return list.toArray(new ImageRegistration[ list.size() ]);
		}
	}

	private void updateSystems(final ImageRegistration[ ] registrations)
	{
		final long t0 = System.currentTimeMillis();
		final Map<String, List<ImageRegistration>> map = new TreeMap<>();//TODO remove
		for (int i = 0; i < systems.length; i++)
		{
			final CheckSystem system = systems[ i ];
			try
			{
				final ArrayList<ImageRegistration> value = new ArrayList<>();
				if (!system.getWorkTask().equals(WorkTask.NONE))
				{
					map.put(system.getName(), value);

					for (final ImageRegistration reg : registrations)
					{
						if (reg.owner == i)
						{
							if (!system.getName().equals(reg.systemName))
							{
								System.err.println("Wrong nme " + system.getName() + ", " + reg.systemName);
							}
							value.add(reg);
							if (reg == null)
							{
								throw new NullPointerException();
							}
						}
					}
					if (!value.isEmpty())
					{
						system.getUpdater().update(value.toArray(new ImageRegistration[ value.size() ]));
					}
				}
			}
			catch (final Exception e)
			{
				e.printStackTrace();
				system.getUpdater().onError("Global thread exception");
			}
		}
//		System.out.println("upateSystems took " + (System.currentTimeMillis() - t0));
	}
}
