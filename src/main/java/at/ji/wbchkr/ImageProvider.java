package at.ji.wbchkr;

import java.io.IOException;

import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public abstract class ImageProvider
{
	public abstract ImageRegistration get(long timeout_ms) throws IOException;

	public abstract boolean isEnabled() throws IOException;

	public abstract void set(String id, String value) throws IOException;

	public abstract void setEnabled() throws IOException;
}
