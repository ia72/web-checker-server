package at.ji.wbchkr.configuration.file;

import at.ji.wbchkr.configuration.Configuration;

public class NoConfigurationFileException extends ConfigurationIoNotSupportedException
{
	
	/**
	 *
	 */
	private static final long serialVersionUID = -3240768060440002926L;
	
	public NoConfigurationFileException(final Configuration configuration, final boolean isSaveAttempt)
	{
		super(configuration, isSaveAttempt);
	}
	
	public NoConfigurationFileException(final String explanation, final Configuration configuration,
		final boolean isSaveAttempt)
	{
		super(explanation, configuration, isSaveAttempt);
	}
	
}
