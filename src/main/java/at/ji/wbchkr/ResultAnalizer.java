package at.ji.wbchkr;

import at.ji.utils.massives.collections.DequeRevolved;
import at.ji.utils.measures.statistics.encoders.DirectionEncoder;
import at.ji.wbchkr.configuration.file.YamlConfiguration;
import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class ResultAnalizer
{
	public static class CounterManager
	{

		private final CheckSystem system;
		private final DirectionEncoder encoder;

		public CounterManager(final CheckSystem system, final int encoderCount)
		{
			this.system = system;
			encoder = new DirectionEncoder(encoderCount);
		}

		public double getCount()
		{
			return encoder.getCount();
		}

		public CheckSystem getSystem()
		{
			return system;
		}

		public void update(final double[ ] activations, final double level)
		{
			encoder.update(activations, level);
		}
	}

	private final CheckSystem system;
	private final CounterManager counterManager;
	private final DequeRevolved<Double> analized_max = new DequeRevolved<>(0);
	private final DequeRevolved<Double> analized_avg = new DequeRevolved<>(0);

	public ResultAnalizer(final CheckSystem system)
	{
		super();
		this.system = system;
		counterManager = new CounterManager(system, system.getConfiguration().getInt("provyazka.encoderSteps"));
	}

	public synchronized void onNewResult(final ImageRegistration reg)
	{
		//====================================================== provyazka
		counterManager.update(reg.provyazkaResult, system.getConfiguration().getInt("provyazka.level"));

		//====================================================== signal
// 		try
		{
			final YamlConfiguration config = system.getConfiguration();
			final double lvlvg = config.getDouble("nn.level.average");
			final double lvlMax = config.getDouble("nn.level.max");
			final double lvlOutput = config.getDouble("nn.level.output");
			analized_avg.insert(reg.getResult()[ 0 ]);
			analized_max.insert(reg.getResult()[ 1 ]);
//			state.analized.add(res); //TODO
		}
	}

	public synchronized void reset()
	{
		analized_avg.clear();
		analized_max.clear();
		final int capacity = system.getConfiguration().getInt("nn.images");
		analized_avg.setCapacity(capacity);
		analized_max.setCapacity(capacity);
	}
}
