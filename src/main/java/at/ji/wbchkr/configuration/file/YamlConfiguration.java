package at.ji.wbchkr.configuration.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.representer.Representer;

import at.ji.wbchkr.configuration.Configuration;
import at.ji.wbchkr.configuration.ConfigurationSection;
import at.ji.wbchkr.configuration.InvalidConfigurationException;

/**
 * An implementation of {@link Configuration} which saves all files in Yaml.
 * Note that this implementation is not synchronized.
 */
public class YamlConfiguration extends FileConfiguration
{
	protected static final String COMMENT_PREFIX = "# ";

	protected static final String BLANK_CONFIG = "{}\n";
	
	private final DumperOptions yamlOptions = new DumperOptions();
	private final Representer yamlRepresenter = new YamlRepresenter();
	private final Yaml yaml = new Yaml(new YamlConstructor(), yamlRepresenter, yamlOptions);
	
	public YamlConfiguration()
	{
		super();
	}
	public YamlConfiguration(final File file)
	{
		super(file);
	}
	
	@Override
	public void loadFromString(final String contents) throws InvalidConfigurationException
	{
		Validate.notNull(contents, "Contents cannot be null");
		
		Map<?, ?> input;
		try
		{
			input = (Map<?, ?>) yaml.load(contents);
		}
		catch (final YAMLException e)
		{
			throw new InvalidConfigurationException(e);
		}
		catch (final ClassCastException e)
		{
			throw new InvalidConfigurationException("Top level is not a Map.");
		}
		
		final String header = parseHeader(contents);
		if (header.length() > 0)
		{
			options().header(header);
		}
		
		if (input != null)
		{
			convertMapsToSections(input, this);
		}
	}
	
	@Override
	public YamlConfigurationOptions options()
	{
		if (options == null)
		{
			options = new YamlConfigurationOptions(this);
		}
		
		return (YamlConfigurationOptions) options;
	}
	
	@Override
	public String saveToString()
	{
		yamlOptions.setIndent(options().indent());
		yamlOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
		yamlRepresenter.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
		
		final String header = buildHeader();
		String dump = yaml.dump(getValues(false));
		
		if (dump.equals(YamlConfiguration.BLANK_CONFIG))
		{
			dump = "";
		}
		
		return header + dump;
	}
	
	@Override
	protected String buildHeader()
	{
		final String header = options().header();
		
		if (options().copyHeader())
		{
			final Configuration def = getDefaults();
			
			if ((def != null) && (def instanceof FileConfiguration))
			{
				final FileConfiguration filedefaults = (FileConfiguration) def;
				final String defaultsHeader = filedefaults.buildHeader();
				
				if ((defaultsHeader != null) && (defaultsHeader.length() > 0))
				{
					return defaultsHeader;
				}
			}
		}
		
		if (header == null)
		{
			return "";
		}
		
		final StringBuilder builder = new StringBuilder();
		final String[ ] lines = header.split("\r?\n", -1);
		boolean startedHeader = false;
		
		for (int i = lines.length - 1; i >= 0; i--)
		{
			builder.insert(0, "\n");
			
			if ((startedHeader) || (lines[ i ].length() != 0))
			{
				builder.insert(0, lines[ i ]);
				builder.insert(0, YamlConfiguration.COMMENT_PREFIX);
				startedHeader = true;
			}
		}
		
		return builder.toString();
	}
	
	protected void convertMapsToSections(final Map<?, ?> input, final ConfigurationSection section)
	{
		for (final Map.Entry<?, ?> entry : input.entrySet())
		{
			final String key = entry.getKey().toString();
			final Object value = entry.getValue();
			
			if (value instanceof Map)
			{
				convertMapsToSections((Map<?, ?>) value, section.createSection(key));
			}
			else
			{
				section.set(key, value);
			}
		}
	}
	
	protected String parseHeader(final String input)
	{
		final String[ ] lines = input.split("\r?\n", -1);
		final StringBuilder result = new StringBuilder();
		boolean readingHeader = true;
		boolean foundHeader = false;
		
		for (int i = 0; (i < lines.length) && (readingHeader); i++)
		{
			final String line = lines[ i ];
			
			if (line.startsWith(YamlConfiguration.COMMENT_PREFIX))
			{
				if (i > 0)
				{
					result.append("\n");
				}
				
				if (line.length() > YamlConfiguration.COMMENT_PREFIX.length())
				{
					result.append(line.substring(YamlConfiguration.COMMENT_PREFIX.length()));
				}
				
				foundHeader = true;
			}
			else if ((foundHeader) && (line.length() == 0))
			{
				result.append("\n");
			}
			else if (foundHeader)
			{
				readingHeader = false;
			}
		}
		
		return result.toString();
	}
	
	/**
	 * Creates a new {@link YamlConfiguration}, loading from the given file.
	 * <p>
	 * Any errors loading the Configuration will be logged and then ignored.
	 * If the specified input is not a valid config, a blank config will be
	 * returned.
	 * <p>
	 * The encoding used may follow the system dependent default.
	 *
	 * @param file Input file
	 * @return Resulting configuration
	 * @throws IllegalArgumentException Thrown if file is null
	 */
	public static YamlConfiguration loadConfiguration(final File file)
	{
		Validate.notNull(file, "File cannot be null");
		
		final YamlConfiguration config = new YamlConfiguration();
		
		try
		{
			config.load(file);
		}
		catch (final FileNotFoundException ex)
		{}
		catch (final IOException ex)
		{
			ex.printStackTrace();
		}
		catch (final InvalidConfigurationException ex)
		{
			ex.printStackTrace();
		}
		
		return config;
	}
	
	/**
	 * Creates a new {@link YamlConfiguration}, loading from the given reader.
	 * <p>
	 * Any errors loading the Configuration will be logged and then ignored.
	 * If the specified input is not a valid config, a blank config will be
	 * returned.
	 *
	 * @param reader input
	 * @return resulting configuration
	 * @throws IllegalArgumentException Thrown if stream is null
	 */
	public static YamlConfiguration loadConfiguration(final Reader reader)
	{
		Validate.notNull(reader, "Stream cannot be null");
		
		final YamlConfiguration config = new YamlConfiguration();
		
		try
		{
			config.load(reader);
		}
		catch (final IOException ex)
		{
			ex.printStackTrace();
		}
		catch (final InvalidConfigurationException ex)
		{
			ex.printStackTrace();
		}
		
		return config;
	}
}
