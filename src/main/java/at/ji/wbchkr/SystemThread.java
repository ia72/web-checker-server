package at.ji.wbchkr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import at.ji.utils.massives.collections.DequeRevolved;
import at.ji.wbchkr.CheckSystem.WorkTask;
import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class SystemThread extends Thread
{
	private final CheckSystem system;
	private final Object lock = new Object();
	private volatile boolean enabled = false;
	private final List<ImageRegistration> toWrite = new ArrayList<>();

	public SystemThread(final CheckSystem system)
	{
		super();
		this.system = system;
		setName("System thread of " + system.getName());
	}

	@Override
	public void run()
	{
		final Thread t = new Thread("Watchdog thread of " + system.getName())
		{
			@Override
			public void run()
			{
				while (SystemThread.this.getState().equals(State.RUNNABLE))
				{
					if (2000 < System.currentTimeMillis() - system.getGlobalThreadLastTick())
					{
						system.getUpdater().onError("");
						System.err.println("Global thread inactive detected");
					}
					if (2000 < System.currentTimeMillis() - system.getSystenThreadLastTick())
					{
						system.getUpdater().onError("");
						System.err.println("System " + system.getName() + " thread inactive detected");
					}
					try
					{
						Thread.sleep(150);
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}
				}
				System.out.println("Exit " + Thread.currentThread().getName());
			}
		};
		t.start();
		BufferedImage img = new BufferedImage(1, 1, 5);
		enabled = true;
		while (enabled)
		{
			double shouldSleepMillis = 0;
			final long t0 = System.currentTimeMillis();
			try
			{
				final ImageProvider imgsrc = system.getImageProvider();
				if (imgsrc == null)
				{
					try
					{
						Thread.sleep(1);
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}
					continue;
				}
				if (imgsrc.isEnabled())
				{
					final DequeRevolved<ImageRegistration> deque = getDeque();
					final int sz = deque.size();
					final int cp = deque.getCapacity();
					boolean wasFull = false;
//					System.out
//							.println(
//									deque.size() + " " + deque.isFull() + " " + deque.getCapacity() + " "
//											+ (deque.getCapacity() <= deque.size()));
					if (deque.isFull())
					{
						wasFull = true;
						synchronized (deque)
						{
							try
							{
								deque.removeFirst();
							}
							catch (final Exception e)
							{
								System.err.println("Dequeue capacity " + deque.getCapacity() + " size " + deque.size());
								throw e;
							}
							if (Math.random() < 0.1)
							{
								System.err.println("Lag " + deque.size());
							}
						}

						debug("System thread + " + system.getName() + " queue full");
					}

					if (!wasFull)
					{
						final ImageRegistration e = imgsrc.get(-1);
						e.systemName = system.getName();
						e.needAnalysis = system.getWorkTask().equals(WorkTask.WORK);
						img = e.image;
//						e.owner = system.getConfiguration().getString("name");
						if (e.data == null)
						{
							throw new NullPointerException();
						}
						synchronized (deque)
						{
							deque.add(e);
						}

						shouldSleepMillis = system.getConfiguration().getInt("fps") - (e.time - t0);
						debug("System thread + " + system.getName() + " got image");
					}
				}
			}
			catch (final IOException e)
			{
				try
				{
					Thread.sleep(50);
				}
				catch (final Exception e2)
				{
					e.printStackTrace();
				}
				throw new RuntimeException(e);
			}

			if (0 < shouldSleepMillis)
			{
				try
				{
					Thread.sleep((long) shouldSleepMillis);
					System.out.println("System thread sleep " + shouldSleepMillis);
				}
				catch (final InterruptedException e2)
				{
					enabled = false;
				}
			}
			if (system.getListener() != null)
			{
				system.systemThreadTickNofity();
				system.getListener().event(system, "info", system.getTextForUi(), img);
			}
			if (system.getConfiguration().getBoolean("debug", false))
			{
				System.out.println("System thread tick");
			}
			synchronized (toWrite)
			{
				for (int i = 0; i < Math.min(2, toWrite.size()); i++)
				{
					final ImageRegistration reg = toWrite.remove(0);
					system.getSessionManager().write(reg);
				}
			}
		}

		exit_();
	}

	public void write(final ImageRegistration[ ] regs)
	{
		synchronized (toWrite)
		{
			toWrite.addAll(Arrays.asList(regs));
		}

	}

	private void debug(final String msg)
	{
		if (system.getConfiguration().getBoolean("debug", false))
		{
			System.out.println(msg);
		}
	}

	private void exit_()
	{
		try
		{
			if (system.getListener() != null)
			{
				system.getListener().event(system, "exit", "", null);
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		//TODO exit(1);
	}

	private DequeRevolved<ImageRegistration> getDeque()
	{
		return system.getDeque();
	}
}
