package at.ji.wbchkr.nn;

import java.awt.Frame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Function;

import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.memory.MemoryWorkspace;
import org.nd4j.linalg.api.memory.conf.WorkspaceConfiguration;
import org.nd4j.linalg.api.memory.enums.AllocationPolicy;
import org.nd4j.linalg.api.memory.enums.LearningPolicy;
import org.nd4j.linalg.api.memory.enums.LocationPolicy;
import org.nd4j.linalg.api.memory.enums.MirroringPolicy;
import org.nd4j.linalg.api.memory.enums.ResetPolicy;
import org.nd4j.linalg.api.memory.enums.SpillPolicy;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.api.ops.impl.layers.convolution.Pooling2D.Pooling2DType;
import org.nd4j.linalg.api.ops.impl.layers.convolution.config.Pooling2DConfig;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.nd4j.linalg.jcublas.buffer.CudaByteDataBuffer;

import at.ji.utils.deeplearning4j.iterators.uri.UriDataSetIterator;
import at.ji.utils.deeplearning4j.realtime.loaders.image.IllegalIndArrayException;
import at.ji.utils.deeplearning4j.realtime.loaders.image.PadImageLoader;
import at.ji.utils.deeplearning4j.realtime.loaders.image.SimplePadChecker;
import at.ji.utils.deeplearning4j.realtime.loaders.image.ZoomImageLoader;
import at.ji.utils.deeplearning4j.visualization.VisualizationListenerFrame;
import at.ji.wbchkr.nn.Analizer.Config.AnalizeMode;

public class Analizer
{
	public static class Config
	{
		public enum AnalizeMode
		{
			BY_CLASS, BY_MARK
		}

		public double dropout = 0;
		private double level = 0;
		public boolean detectDeffects;
		public boolean detectProvyazka;
		public File saveImagesFile;
		public AnalizeMode analizeMode = AnalizeMode.BY_CLASS;
		private SimplePadChecker checker;

		public static Config create(final double dropout, final double level, final boolean detectDeffects,
				final boolean detectProvyazka, final File saveImagesFile, final AnalizeMode analizeMode,
				final double minBrightness, final double maxBrightness, final double diffBrightness)
		{
			final Config config = new Config();
			config.dropout = dropout;
			config.level = level;
			config.detectDeffects = detectDeffects;
			config.detectProvyazka = detectProvyazka;
			config.analizeMode = analizeMode;
			config.checker = new SimplePadChecker(true, false, minBrightness, maxBrightness, diffBrightness);
			return config;
		}
	}

	public static class ImageRegistration
	{
		public enum AnalizationMode
		{
			BY_MARK, BY_CLASS
		}

		public BufferedImage image;
		public byte[ ] data;
		public long time;
		public double[ ] result;
		public double[ ] markOutput;//check
		public double[ ] provyazkaResult;
		public int outputWidth;
		public double[ ] classOutput;
		public int owner;
		public int scaleW = 2;
		public int scaleH = 2;
		public boolean hasOffset = false;
		public int bagNumber = 0;
		public String systemName = "UNKNOWN";
		public boolean needAnalysis = false;

		public ImageRegistration(final BufferedImage image, final byte[ ] data, final long time)
		{
			super();
			this.image = image;
			this.data = data;
			this.time = time;
		}

		public BufferedImage getFirst()
		{
			return image;
		}

		public double[ ] getOutput()
		{
			return markOutput;
		}

		public int getOutputWidth()
		{
			return outputWidth;
		}

		public double[ ] getResult()
		{
			return result;
		}

		public byte[ ] getSecond()
		{
			return data;
		}

	}

	private static final DataType DATA_TYPE = DataType.FLOAT;

	private static NativeImageLoader nativeImageLoader;
	private ComputationGraph graph;
	private final ComputationGraph graphProvyazka;
	private final VisualizationListenerFrame frame;
	private final VisualizationListenerFrame frame2;
	private final int minibatchSize;

	private final VisualizationListenerFrame frameSupport;

	private BufferedImage image;

	private boolean allowDrawSupport = true;

	long sleepDebug = 0;
	private final ArrayList<String> toPrint = new ArrayList<>();

	public Analizer(final File file, final int minibatchSize, final String systemName)
	{

		frame = new VisualizationListenerFrame("Data " + systemName);
//		if (!systemName.equalsIgnoreCase("TEST"))
//		{
//			frame.getVisualizationPanel().getDrawController().setSelected(true);
//			frame.getVisualizationPanel().getSettingsPanel().setVisible(false);
//		}
		frame.setState(Frame.ICONIFIED);
		frame.setVisible(true);
		frame2 = new VisualizationListenerFrame("Data provyazka " + systemName);
		frame2.setState(Frame.ICONIFIED);
		frame2.setVisible(true);
		frameSupport = new VisualizationListenerFrame("���������� " + systemName);
//		frameSupport.setVisible(true);
//		frameSupport.setBounds(720, 100, 480, 640);
		this.minibatchSize = minibatchSize;
		try
		{
//			//print(DataTypeUtil.getDtypeFromContext());
//			//print(Nd4j.defaultFloatingPointType());
			Nd4j.setDefaultDataTypes(Analizer.DATA_TYPE, Analizer.DATA_TYPE);
			Analizer.printMemoryInfo();
			setAgent(file);
			ComputationGraph graphProvyazka1;
			try
			{
				graphProvyazka1 = ComputationGraph.load(new File("data/provyazkaHelper.nn"), false);
				graphProvyazka1.init();
			}
			catch (final Exception e)
			{
				System.err.println("Cannot load provyazka helper!");
				e.printStackTrace();
				graphProvyazka1 = null;
			}
			graphProvyazka = graphProvyazka1;
//			graphProvyazka.conf().setDataType(dataType);
			Analizer.printMemoryInfo();
//			//print(graph.memoryInfo(8, InputType.convolutional(360, 430, 1)));
			Nd4j.getWorkspaceManager().destroyAllWorkspacesForCurrentThread();
			final int cnt = 20;
			for (int i = 0; i < cnt; i++)
			{
				System.gc();
				Nd4j.getMemoryManager().invokeGc();

			}
		}
		catch (final Exception e)
		{

			throw new RuntimeException(e);
		}
	}

	public void analyze(final ImageRegistration[ ] parameterObject, final Config config) throws IllegalIndArrayException//TODO add dimension to array
	{
		synchronized (this)
		{

			final IllegalIndArrayException exception = null;
			print("Skip " + (System.currentTimeMillis() - sleepDebug));
			final long t0 = System.currentTimeMillis();
			final MemoryWorkspace ws = getWorkspace();

			if (ws.isScopeActive())
			{
				ws.notifyScopeLeft();
			}
			ws.notifyScopeEntered();

			final int width = parameterObject[ 0 ].getFirst().getWidth();
			final int height = parameterObject[ 0 ].getFirst().getHeight();
			getLoader(width, height);

//		final List<INDArray[ ]> load = load(parameterObject);
			print("Load took " + (System.currentTimeMillis() - t0));

			final List<Entry<BufferedImage, double[ ]>> results = new ArrayList<>(parameterObject.length * 2);

			final INDArray[ ][ ] inputs;

			final long t1 = System.currentTimeMillis();
//		inputs = load(load);
			//print("Load1 took " + (System.currentTimeMillis() - t1));

			INDArray[ ] inputs1;
			final long t2 = System.currentTimeMillis();
			inputs1 = loadFast(parameterObject);
			print("Stack took " + (System.currentTimeMillis() - t2));

			final org.nd4j.linalg.dataset.MultiDataSet multiDataSet = new org.nd4j.linalg.dataset.MultiDataSet(
					inputs1,
					new INDArray[ 0 ]);
//		Train.PREPROCESSOR.preProcess(multiDataSet);
			inputs1 = multiDataSet.getFeatures();
			if (config.saveImagesFile != null)
			{
				try
				{
					System.out.println("Saving " + config.saveImagesFile);
					Nd4j.writeAsNumpy(inputs1[ 0 ], config.saveImagesFile);
				}
				catch (final IOException e)
				{
					e.printStackTrace();
				}
			}
//		//print("Packing took " + (System.currentTimeMillis() - t0));
			try
			{
				Thread.sleep(0);
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
//		frame2.onForwardPass(graph, treeMap);
			print("With processing took " + (System.currentTimeMillis() - t0));

			final long t3 = System.currentTimeMillis();

			if (config.detectProvyazka)
			{
				final INDArray indArray = inputs1[ 0 ];
				final int scaleW = 1;//parameterObject[ 0 ].scaleW;
				final int scaleH = 1;//parameterObject[ 0 ].scaleH;
				final int dw = 28 / scaleW;
				final int dh = 360 / scaleH;
				final int yMid0 = (int) (indArray.size(2) * 0.5);
//			final int yMid1 = (int) (indArray.size(2) * 0.6);
				final long i0 = yMid0 - scaleH * dh;
				final long i1 = yMid0 + scaleH * dh;
				INDArray inputpCnnProvyazka = indArray.get(NDArrayIndex.all(), NDArrayIndex.all(), NDArrayIndex.all(),
//								NDArrayIndex.interval(i0, i1),
						NDArrayIndex
								.interval(
										indArray.size(3) / 2 - scaleW * dw / 2,
										indArray.size(3) / 2 + scaleW * dw / 2));
//			final long i2 = yMid1 - scaleH * dh;
//			final long i3 = yMid1 + scaleH * dh;
//			INDArray inputp1 = indArray
//				.get(
//					NDArrayIndex.all(),
//					NDArrayIndex.all(),
//					NDArrayIndex.interval(i2, i3),
//					NDArrayIndex.interval(indArray.size(3) / 2 - scaleW * dw, indArray.size(3) / 2 + scaleW * dw));
				if (scaleH != 1 || scaleW != 1)
				{
					inputpCnnProvyazka = Nd4j
							.cnn()
							.avgPooling2d(
									inputpCnnProvyazka,
									Pooling2DConfig.builder().kW(scaleW).kH(scaleH).sW(scaleW).sH(scaleH).build());
//			inputp1 = Nd4j
//				.cnn()
//				.avgPooling2d(inputp1, Pooling2DConfig.builder().kW(scaleW).kH(scaleH).sW(scaleW).sH(scaleH).build());
				}

				final long t4p = System.currentTimeMillis();
				final INDArray[ ] o0 = graphProvyazka.output(false, ws, inputpCnnProvyazka);
				print("Provyazka  took " + (System.currentTimeMillis() - t4p));
//			final INDArray[ ] o1 = graphProvyazka.output(false, ws, inputp1);
//			final INDArray o10 = o1[ 0 ];
				final INDArray o00 = o0[ 0 ];
				final int size = (int) o00.size(0);
				final Map<String, INDArray> treeMap = new TreeMap<>();
				treeMap.put("input0", inputpCnnProvyazka);
//			treeMap.put("input1", inputp1);
				treeMap.put("output0", o00);
//			treeMap.put("output1", o10);
				for (int i = 0; i < size; i++)
				{
					final double[ ] d = new double[ (int) o00.size(1) ];
					for (int j = 0; j < d.length; j++)
					{
						d[ j ] = o00.getDouble(i * 12 + j);
					}
					parameterObject[ i ].provyazkaResult = d;
				}
				frame2.onForwardPass(null, treeMap);
			}
			final long t0other = System.currentTimeMillis();
			if (config.detectDeffects)
			{
//				checkEdge(config, inputs1, parameterObject);//FIXME
				final long t4 = System.currentTimeMillis();
				final INDArray[ ] outputs = ouput640(graph, ws, inputs1, config.dropout);
				print("Output  took " + (System.currentTimeMillis() - t4));
				//print("Output took " + (System.currentTimeMillis() - t3));
				final Map<String, INDArray> treeMap = new TreeMap<>();
				treeMap.put("input", inputs1[ 0 ]);
				treeMap.put("mark", outputs[ 1 ]);
				treeMap.put("class", outputs[ 0 ]);
				final INDArray markOutput = treeMap.get("mark");
				final INDArray classOutput = treeMap.get("class");

				final INDArray[ ] output = new INDArray[ ]
				{ markOutput };//graph.output(inputs1);
				{
					final long kw = markOutput.shape()[ 3 ];
					final long kh = markOutput.shape()[ 2 ];
//				final Pooling2DConfig poolingConfig = Pooling2DConfig.builder().kW(kw).kH(kh).sW(kw).sH(kh).build();
					final INDArray max = markOutput.max(1, 2, 3);
					final INDArray avg = markOutput.mean(1, 2, 3);
					final long oWidth = markOutput.size(3);
//			final Pair<Pair<BufferedImage, byte[ ]>>[ ] imgArray = inputImages.toArray(new Pair[ 0 ]);

					final int sz = (int) (markOutput.size(2) * oWidth);
					final long mbs = markOutput.shape()[ 0 ];
					for (int i = 0; i < mbs; i++)
					{
						final int index = i;
						final double maxFixed = max.getDouble(i) + config.dropout;
						final double[ ] d =
						{ config.level == 0 ? maxFixed : config.level <= maxFixed ? 1 : 0, avg.getDouble(i) };

						if (config.analizeMode.equals(AnalizeMode.BY_CLASS))
						{
							d[ 0 ] = classOutput.getDouble(i, 1);
						}

						parameterObject[ index ].result = d;
						final INDArray oBatched = markOutput
								.get(
										NDArrayIndex.interval(i, i + 1),
										NDArrayIndex.all(),
										NDArrayIndex.all(),
										NDArrayIndex.all());
						final INDArray prod2 = oBatched.prod(2);
						final INDArray prod = prod2.reshape(prod2.size(0), prod2.size(1), 1, prod2.size(2));
						final long kW2 = prod.size(3) / 20;
						final INDArray smallOutputBatched = Nd4j
								.cnn()
								.avgPooling2d(
										prod,
										Pooling2DConfig
												.builder()
												.kW(kW2)
												.sW(kW2)
												.type(Pooling2DType.AVG)
												.isSameMode(false)
												.build());
						final double[ ] arr = smallOutputBatched.reshape(smallOutputBatched.length()).toDoubleVector();
						parameterObject[ index ].markOutput = arr;
						parameterObject[ index ].outputWidth = arr.length;
					}
				}

//			if (graphProvyazka != null)
//			{
//				final Map<String, INDArray> treeMap2 = graphProvyazka.feedForward(inputs1, false);
////			frameSupport.onForwardPass(graph2, treeMap2);
//				o2.muli(treeMap2.get("markOutput"));
//			}

//			int scale = 0;//FIXME
//			scale = (int) (inputs1[0].size(3) / o.size(3));
//			inputs1[ 0 ]
//				.muli(2)
//				.addi(
//					Nd4j.cnn
//						.upsampling2d(o, scale)
//						.get(
//							NDArrayIndex.all(),
//							NDArrayIndex.all(),
//							NDArrayIndex.interval(0, 360),
//							NDArrayIndex.interval(0, 640)))
//				.divi(2);

//		//print("Took " + (System.currentTimeMillis() - t0));
//		image = UtilsGraphics2Dnd4j
//			.toSimpleImage(
//				inputs1[ 0 ]
//					.addi(
//						Nd4j.cnn
//							.upsampling2d(o.muli(-1), 8)
//							.get(
//								NDArrayIndex.all(),
//								NDArrayIndex.all(),
//								NDArrayIndex.all(),
//								NDArrayIndex.interval(0, 430)))
//					.muli(0xff));
				frame.onForwardPass(graph, treeMap);
				if (graphProvyazka == null)
				{
					if (frameSupport.isVisible())
					{
						if (allowDrawSupport)
						{
//						frameSupport.onForwardPass(graph, treeMap);
						}
					}
				}
			}
			print("Other (no gc) took " + (System.currentTimeMillis() - t0other));
			ws.notifyScopeLeft();
			ws.destroyWorkspace();
//			Nd4j.getMemoryManager().invokeGcOccasionally();
			print("Other (with gc) took " + (System.currentTimeMillis() - t0other));
			print("Total took " + (System.currentTimeMillis() - t0));
			printFlush();
			sleepDebug = System.currentTimeMillis();
			if (exception != null)
			{
				throw exception;
			}
		}
	}

	public BufferedImage getImage()
	{
		return image;
	}

	public MemoryWorkspace getWorkspace()
	{
		if (Nd4j.getWorkspaceManager().checkIfWorkspaceExists("WcWs"))

		{
			return Nd4j.getWorkspaceManager().getWorkspaceForCurrentThread("WcWs");
		}
		final WorkspaceConfiguration workspaceConfiguration = Analizer.getWorkspaceConfiguration();
		print("Starting workspace " + workspaceConfiguration);
		return Nd4j.getWorkspaceManager().getWorkspaceForCurrentThread(workspaceConfiguration, "WcWs");
	}

	public void setAgent(final File file) throws IOException
	{
		synchronized (this)
		{
			System.out.println("�������� " + file);
			graph = ComputationGraph.load(file, false);
			graph.conf().setDataType(Analizer.DATA_TYPE);
			graph.init();
			Nd4j.getMemoryManager().invokeGc();
			Nd4j.getMemoryManager().invokeGc();
			System.out.println("����� " + file);
		}
	}

	public void setAllowDrawSupport(final boolean allowDrawSupport)
	{
		this.allowDrawSupport = allowDrawSupport;
	}

	private void checkEdge(final Config config, final INDArray[ ] inputs1, final ImageRegistration[ ] regs)
	{
		final INDArray indArray = inputs1[ 0 ];
		int i = 0;
		int i0 = 0;
		String systemName = regs[ 0 ].systemName;
		while (i < indArray.size(0))
		{
			if (systemName.equals(regs[ i ].systemName))
			{

			}
			else
			{
				try
				{
					System.out.println("Checking edge " + i + ", " + i0);
					final INDArray array = indArray
							.get(
									NDArrayIndex.interval(i0, i - 1),
									NDArrayIndex.all(),
									NDArrayIndex.all(),
									NDArrayIndex.all());
					config.checker.assertArray(array);//TODO separate edge checking
				}
				catch (final IllegalIndArrayException e1)
				{
					regs[ i0 ].hasOffset = true;
				}
				i0 = i;
				systemName = regs[ i ].systemName;
			}
			i++;
		}

	}

	private void evaluate()
	{

		final String path = "C:\\Files\\Development\\Repositories\\git\\at.ji.local.0\\neuralData\\nndatarev1";
		final MultiDataSetPreProcessor preprocessor = new MultiDataSetPreProcessor()
		{
			@Override
			public void preProcess(final MultiDataSet multiDataSet)
			{
//				final INDArray[ ] l = new INDArray[ ] { Nd4j.create(new float[ ] { 0 }, new int[ ] { 1, 1 }) };
//				multiDataSet.setLabels(l);
			}
		};
		final URI uri = new File(path).toURI();
		final UriDataSetIterator.Builder builder = new UriDataSetIterator.Builder(uri)
				.preprocessor(preprocessor)
				.cache(0)
				.repeat(1)
				.batch(1);
		final MultiDataSetIterator test = builder.jsonImage("test", new Function<URI, INDArray[ ]>()
		{

			@Override
			public INDArray[ ] apply(final URI t)
			{

				final NativeImageLoader nativeImageLoader = new NativeImageLoader(720, 1280, 1);

				try
				{

					return new INDArray[ ]
					{ nativeImageLoader.asMatrix(new File(t)).divi(255F) };
				}
				catch (final Exception e)
				{

					throw new RuntimeException(e);
				}
			}
		}).preprocessor(preprocessor).repeat(1).cache(0).build().pack(4);
		System.out.println("Evaluate model....");
		final Evaluation eval = graph.evaluate(test);
		System.out.println(eval.stats());
		final NativeImageLoader var = new org.datavec.image.loader.NativeImageLoader();
		System.out.println("****************Example finished********************");
	}

	private void getLoader(final int width, final int height)
	{
		//no op
	}

	private WorkspaceConfiguration getWsConfig()//unused
	{
		return WorkspaceConfiguration
				.builder()
				.policyLearning(LearningPolicy.OVER_TIME)
				.policySpill(SpillPolicy.REALLOCATE)
				.policyMirroring(MirroringPolicy.HOST_ONLY)
				.policyLocation(LocationPolicy.RAM)
				.policyReset(ResetPolicy.BLOCK_LEFT)
				.policyAllocation(AllocationPolicy.OVERALLOCATE)
				.cyclesBeforeInitialization(8)
				.overallocationLimit(1)
				.initialSize(1024L * 1024L * 512L)
				.minSize(1024L * 1024L * 512L)
				.maxSize(1024L * 1024L * 4096L)
				.build();
	}

	private INDArray[ ] loadFast(final ImageRegistration[ ] regs)
	{
		return new INDArray[ ]
		{ load640(regs) };
	}

	private void print(final String string)
	{
		toPrint.add(string);

	}

	private void printFlush()
	{
		String text = "===========";
		for (final String s : toPrint)
		{
			text += System.lineSeparator() + s;
		}
//		System.out.println(text);
		toPrint.clear();

	}

	public static INDArray fixOutput(INDArray markRaw, final boolean inPlace)
	{
		markRaw = markRaw.get(NDArrayIndex.all(), NDArrayIndex.interval(0, 1), NDArrayIndex.all(), NDArrayIndex.all());
		if (!inPlace)
		{
			markRaw = markRaw.dup();
		}
//		BooleanIndexing.replaceWhere(markRaw, 0.0, Conditions.lessThan(0.0));
//		BooleanIndexing.replaceWhere(markRaw, 2.0, Conditions.greaterThan(2.0));
		limit(markRaw);
		return markRaw;
	}

	public static MemoryWorkspace getWorkspace(final String id)
	{
		if (Nd4j.getWorkspaceManager().checkIfWorkspaceExists(id))
		{
			return Nd4j.getWorkspaceManager().getWorkspaceForCurrentThread(id);
		}
		return Nd4j.getWorkspaceManager().createNewWorkspace(Analizer.getWorkspaceConfiguration(), id);
	}

	public static WorkspaceConfiguration getWorkspaceConfiguration()
	{
		return WorkspaceConfiguration
				.builder()
				.policyAllocation(AllocationPolicy.STRICT)
				.policyMirroring(MirroringPolicy.FULL)
				.policyLearning(LearningPolicy.NONE)
				.policyReset(ResetPolicy.BLOCK_LEFT)
				.policyLearning(LearningPolicy.NONE)
				.initialSize(1024L * 1024L * 1024L)
				.minSize(1024L * 1024L * 1024L)
				.maxSize(6000L * 1024L * 1024L)
				.build();
	}

	public static void limit(final INDArray array)
	{
		Nd4j
				.exec(
						new org.nd4j.linalg.api.ops.impl.transforms.custom.Max(
								array,
								Nd4j.zeros(array.dataType(), 1),
								array));
		Nd4j.exec(new org.nd4j.linalg.api.ops.impl.transforms.custom.Min(array, Nd4j.ones(array.dataType(), 1), array));
	}

	public static INDArray load640(final ImageRegistration[ ] regs)
	{
		final Pooling2DConfig config = new Pooling2DConfig();
		config.setKW(2);
		config.setKH(2);
		config.setSW(2);
		config.setSH(2);

		final List<INDArray> list = new ArrayList<>(regs.length * 2);
		final long h = regs[ 0 ].image.getHeight();
		final long w = regs[ 0 ].image.getWidth();
		for (final ImageRegistration r : regs)
		{
			if (r.data == null)
			{
				throw new NullPointerException();
			}
			final CudaByteDataBuffer buffer = new CudaByteDataBuffer(
					ByteBuffer.wrap(r.data),
					DataType.UINT8,
					r.data.length,
					0);

			final org.nd4j.linalg.api.buffer.DataBuffer createBuffer = Nd4j.createBuffer(buffer, 0L, r.data.length);

//			System.out.println(createBuffer.dataType().toString());
			final INDArray create = Nd4j.create(createBuffer, 1, 1, h, w).castTo(DataType.UINT8);//FIXME castTo() unrequired
//			System.out.println(create.minNumber().doubleValue() + " " + create.maxNumber().doubleValue());
			list.add(create);
		}

		final INDArray stack = Nd4j.stack(0, list.toArray(new INDArray[ 0 ]));
		INDArray castTo = stack.castTo(Nd4j.dataType()).reshape(regs.length, 1, h, w);
		if (castTo.size(2) != 360 && castTo.size(3) != 640)
		{
			castTo = Nd4j.cnn().avgPooling2d(castTo, config);
		}
		final INDArray divi = castTo.divi(0xFF);

		INDArray array = divi;

//		System.out.println(array.minNumber().doubleValue() + " " + array.maxNumber().doubleValue());
		final int scroll = 10;
		final ZoomImageLoader zoomImageLoader = new ZoomImageLoader(scroll, 0);

		array = zoomImageLoader.load(array);
		array = new PadImageLoader(scroll + 64, 0, PadImageLoader.MODE_NEAREST, 0).load(array);
		return array;
	}

	public static INDArray[ ] ouput640(final ComputationGraph graph, final MemoryWorkspace ws, final INDArray[ ] inputs,
			final double dropout)
	{

		final INDArray[ ] outputs = graph.output(false, ws, inputs);
		if (dropout != 0)
		{
			outputs[ 1 ].subi(dropout);
		}
		outputs[ 1 ] = fixOutput(outputs[ 1 ], false);
		return outputs;
	}

	private static void printMemoryInfo()
	{
		//print("Current physical Mb " + (Pointer.physicalBytes() / (1024 * 1024)));
		//print("Max physical Mb " + (Pointer.maxPhysicalBytes() / (1024 * 1024)));
		//print("Total physical Mb " + (Pointer.totalPhysicalBytes() / (1024 * 1024)));
		//print("Available physical Mb " + (Pointer.availablePhysicalBytes() / (1024 * 1024)));
	}
}
