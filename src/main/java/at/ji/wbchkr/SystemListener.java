package at.ji.wbchkr;

import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent.Cause;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import at.ji.wbchkr.configuration.file.YamlConfiguration;

public class SystemListener extends EventListener
{
	private final CheckSystem system;
	private final SerialManager serialManager;
	private final JFrame frame;
	private final WcImagePanel panel;
	private long lastRepaintImg;

	public SystemListener(final String port, final CheckSystem system)
	{
		this.system = system;
		panel = new WcImagePanel(system);
		frame = new JFrame("�������� ������ " + system.getName());
		if (system != null)
		{
			{
				final YamlConfiguration sc = system.getConfiguration();
				frame.setBounds(sc.getInt("ui.x"), sc.getInt("ui.y"), sc.getInt("ui.w"), sc.getInt("ui.h"));
				panel.setSize(sc.getInt("ui.w"), sc.getInt("ui.h"));
				panel.setMinimumSize(panel.getSize());
				panel.setPreferredSize(panel.getSize());
//				System.out.println(frame.getX());
			}
		}
		frame.setContentPane(panel);
//		frame.setBounds(frame.getContentPane().getBounds());
		frame.getContentPane().setMinimumSize(frame.getContentPane().getSize());
		frame.getContentPane().setPreferredSize(frame.getContentPane().getSize());
		frame.pack();
		frame.setVisible(true);
		frame.requestFocus(Cause.ACTIVATION);
		frame.addComponentListener(new ComponentListener()
		{

			@Override
			public void componentHidden(final ComponentEvent e)
			{

			}

			@Override
			public void componentMoved(final ComponentEvent e)
			{
				updimpl();
			}

			@Override
			public void componentResized(final ComponentEvent e)
			{
				updimpl();
			}

			@Override
			public void componentShown(final ComponentEvent e)
			{

			}

			private void updimpl()
			{
				synchronized (system.getConfiguration())
				{
					final YamlConfiguration sc = ((YamlConfiguration) system.getConfiguration().getDefaults());
//					System.out.println("To " + frame.getX());
					sc.set("ui.x", frame.getX());
					sc.set("ui.y", frame.getY());
					sc.set("ui.w", panel.getWidth());
					sc.set("ui.h", panel.getHeight());
					system.saveConfig();
				}
			}
		});
		serialManager = new SerialManager(system);
	}

	public synchronized void close()
	{
		serialManager.close();
	}

	@Override
	public void event(final CheckSystem system, final String id, final String data, final BufferedImage img)
	{
		/*
		 *
		 * @param system
		 * @param id init, deinit, info, tick (work, wait), count, move (start, stop), stop (deffect, deffect_hard, offset, error), info
		 * @param data
		 * @param img
		 */

		switch (id)
			{
			case "init":
				try
				{
					initialize();
				}
				catch (final Exception e1)
				{
					e1.printStackTrace();
				}
				break;
			case "deinit":
				try
				{
					close();
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
				break;
			case "tick":
				break;
			case "stop":
				serialManager.setStopRequest();
				break;
			case "info":
				updateUi(data, img);
				break;
			}
	}

	@SuppressWarnings("rawtypes")
	public void initialize()
	{
		serialManager.initialize();
	}

	@Override
	public boolean isFine()
	{
		return serialManager.isFine();
	}

	public void onStop()
	{
		System.err.println("STOPPING ILLEGAL METHOD");
		close();
	}

	private void updateUi(final String text, final BufferedImage img)
	{
		panel.setColor(Color.WHITE);
		panel.setBgColor(new Color(0, 0, 0, 127));
		panel.setText(text);
		if (!(System.currentTimeMillis() - lastRepaintImg < 50))
		{
			panel.setSource(img);
			lastRepaintImg = System.currentTimeMillis();
		}
		panel.repaint();
	}
}
