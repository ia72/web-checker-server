package at.ji.wbchkr;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import javax.swing.JOptionPane;

public class Main
{
	public static void main(final String[ ] args) throws IOException
	{

		final File systemsFolder = new File("systems");
		if (!systemsFolder.isDirectory())
		{
			if (!systemsFolder.mkdirs())
			{
				throw new IOException("�� ������� ������� ����� \"systems\"");
			}
		}
		File[ ] systemsContents = systemsFolder.listFiles(new FileFilter()
		{

			@Override
			public boolean accept(final File pathname)
			{
				return pathname.isDirectory();
			}
		});

		if (systemsContents.length == 0)
		{
			systemsContents = new File[ ]
			{ new File("systems/example/") };
			//TODO empty configuration
		}

		final CheckSystem[ ] systems = SystemsInitializator.loadAndTryInitialize(systemsContents);

		for (final CheckSystem system : systems)
		{
			if (!system.isInitialized())
			{
				String name = "???";
				try
				{
					name = system.getConfiguration().getString("name");
				}
				catch (final Exception e)
				{
				}
				System.err.println("������� �� ���������������� " + name);
				final int res = JOptionPane
						.showConfirmDialog(null, "������� �� ���������������� " + name + ", ���������� ��� ��?");
				if (res != JOptionPane.OK_OPTION)
				{
					System.exit(1);
				}
			}
			else
			{
				System.out.println("������� s " + system.getConfiguration().getString("name"));
			}
		}

		final GlobalThread thread = new GlobalThread(systems);
		thread.start();

		System.out.println("Main thread exit");
	}

	public static void restart(final CheckSystem system)
	{
		system.deinitialize();
		system
				.getConfiguration()
				.setDefaults(SystemsInitializator.getConfigRaw(new File("systems." + system.getName() + ".yml")));
		system.initialize();
	}
}
