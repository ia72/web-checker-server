package at.ji.wbchkr;

import java.io.IOException;

import org.webChecker.nd4j.debug.mvs.MVSCamera;

import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class MvsImageProvider extends ImageProvider
{
	private boolean enabled = false;

	private final Object lock = new Object();
	private final MVSCamera camera;

	public MvsImageProvider(final MVSCamera camera)
	{
		this.camera = camera;
	}

	@Override
	public ImageRegistration get(final long timeout_ms) throws IOException
	{
		if (!isEnabled())
		{
			throw new IOException("Camera is disabled");
		}
		if (timeout_ms != -1)
		{
			throw new IllegalArgumentException("timeout is not implemented");
		}
		synchronized (lock)
		{
			try
			{
				final ImageRegistration out = new ImageRegistration(
						camera.getImage(),
						camera.getLastBytes(),
						System.currentTimeMillis());
				return out;
			}
			catch (final Exception e)
			{
				throw new IOException("���������� �� ����� ��������� �����������", e);
			}
		}
	}

	@Override
	public boolean isEnabled()
	{
		synchronized (lock)
		{
			return enabled;
		}
	}

	@Override
	public void set(final String id, final String value) throws NumberFormatException, IOException
	{
		synchronized (lock)
		{
			if (id.equals("fps"))
			{
				camera.setFps(Integer.valueOf(value));
			}
			else if (id.equals("exposure"))
			{
				camera.setExposureTime(Integer.valueOf(value));
			}
			else if (id.equals("binning.horizontal"))
			{
				camera.setBinningHorizontal(Integer.valueOf(value));
			}
			else if (id.equals("offset.vertical"))
			{
				camera.setOffsetVertical(Integer.valueOf(value));
			}
		}
	}

	@Override
	public void setEnabled()
	{
		synchronized (lock)
		{
			try
			{
//				set("exposure", "100");
//				set("fps", "10");
				camera.open();
				System.out.println("Camera opened " + camera.getDeviceInfo().gigEInfo.serialNumber);
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
			enabled = true;
		}

	}

}
