package at.ji.wbchkr;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;

import at.ji.utils.swing.panels.image.ZoomableImagePanel;
import at.ji.wbchkr.configuration.file.YamlConfiguration;

public class WcImagePanel extends ZoomableImagePanel implements MouseWheelListener
{

	/**
	 *
	 */
	private static final long serialVersionUID = 5781540237901539462L;

	private final CheckSystem system;

	private String text = "";
	private Color color = Color.BLACK;
	private Color bgColor = new Color(0xAA000000);

	public WcImagePanel(final CheckSystem system)
	{
		super();
		this.system = system;
		if (system != null)
		{
			{
				final YamlConfiguration sc = system.getConfiguration();
				setSize(640, 360);
				setMinimumSize(getSize());
				setPreferredSize(getSize());
//				setBounds(sc.getInt("ui.x"), sc.getInt("ui.y"), sc.getInt("ui.w"), sc.getInt("ui.h"));
			}
		}
	}

	public Color getBgColor()
	{
		return bgColor;
	}

	public Color getColor()
	{
		return color;
	}

	public String getText()
	{
		return text;
	}

	@Override
	public void paint(final Graphics g)
	{
		super.paint(g);
		g.setColor(bgColor);
		g.fillRect(0, 0, getWidth(), 120);
		g.setColor(color);
		final Font font = new Font("Calibri", Font.PLAIN, 32);
		g.setFont(font);
		{
			final String[ ] split = text.split("\r\n");
			int i = 1;
			for (final String text_ : split)
			{
				g.drawString(text_, 40, 40 * i++);
			}
		}
	}

	public void setBgColor(final Color bgColor)
	{
		this.bgColor = bgColor;
	}

	public void setColor(final Color color)
	{
		this.color = color;
	}

	public void setText(final String text)
	{
		this.text = text;
	}

	public static void main(final String[ ] args)
	{
		final JFrame frame = new JFrame();
		frame.setBounds(100, 100, 700, 700);
		final WcImagePanel z = new WcImagePanel(null);
		frame.setContentPane(z);
		frame.setVisible(true);
		System.out.println("!");
		// while(true);
	}
}
