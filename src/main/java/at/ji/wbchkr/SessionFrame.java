package at.ji.wbchkr;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SessionFrame extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3450572830752519842L;
	private JPanel contentPane;
	private JTextField operatorField;
	private JTextField rollField;

	/**
	 * Create the frame.
	 * @param system 
	 */
	public SessionFrame(final CheckSystem system)
	{
		super("������ ������ " + system.getName());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 452, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		final JLabel lblNewLabel = new JLabel(
				"\u041D\u043E\u043C\u0435\u0440 \u043E\u043F\u0435\u0440\u0430\u0442\u043E\u0440\u0430");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(20, 9, 129, 53);
		contentPane.add(lblNewLabel);

		final JLabel lblNewLabel_1 = new JLabel("\u041D\u043E\u043C\u0435\u0440 \u0440\u0443\u043B\u043E\u043D\u0430");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_1.setBounds(20, 73, 129, 53);
		contentPane.add(lblNewLabel_1);

		final JButton btnNewButton = new JButton("Ok");
		btnNewButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				dispose();
				synchronized (system.getConfiguration())
				{
					system.getConfiguration().getDefaults().set("session.operatorId", operatorField.getText());
					system.getConfiguration().getDefaults().set("session.rollId", rollField.getText());
				}
				synchronized (system.getSessionManager())
				{
					system.getSessionManager().updateNewSession();
				}
			}
		});
		btnNewButton.setBounds(20, 127, 89, 23);
		contentPane.add(btnNewButton);

		final JButton btnNewButton_1 = new JButton("\u041E\u0442\u043C\u0435\u043D\u0430");
		btnNewButton_1.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(final ActionEvent e)
			{
				dispose();
			}
		});
		btnNewButton_1.setBounds(337, 127, 89, 23);
		contentPane.add(btnNewButton_1);

		operatorField = new JTextField(system.getConfiguration().getString("session.operatorId"));
		operatorField.setBounds(154, 11, 270, 53);
		contentPane.add(operatorField);
		operatorField.setColumns(20);

		rollField = new JTextField(system.getConfiguration().getString("session.rollId"));
		rollField.setColumns(20);
		rollField.setBounds(154, 73, 270, 53);
		contentPane.add(rollField);
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[ ] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					final SessionFrame frame = new SessionFrame(null);
					frame.setVisible(true);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
