package org.webChecker.nd4j.debug.mvs;

/***************************************************************************************************
 * @file      SaveImage.java
 * @breif     Use functions provided in MvCameraControlWrapper.jar to save image as JPEG。
 * @author    zhanglei72
 * @date      2020/02/10
 *
 * @warning
 * @version   V1.0.0  2020/02/10 Create this file
 * @since     2020/02/10
 **************************************************************************************************/
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;

import MvCameraControlWrapper.CameraControlException;
import MvCameraControlWrapper.MvCameraControl;
import MvCameraControlWrapper.MvCameraControlDefines;
import MvCameraControlWrapper.MvCameraControlDefines.Handle;
import MvCameraControlWrapper.MvCameraControlDefines.MVCC_INTVALUE;
import MvCameraControlWrapper.MvCameraControlDefines.MV_CC_DEVICE_INFO;
import MvCameraControlWrapper.MvCameraControlDefines.MV_FRAME_OUT_INFO;
import MvCameraControlWrapper.MvCameraControlDefines.MV_SAVE_IAMGE_TYPE;
import MvCameraControlWrapper.MvCameraControlDefines.MV_SAVE_IMAGE_PARAM;

public class SaveImage
{
	public static int chooseCamera(final ArrayList<MV_CC_DEVICE_INFO> stDeviceList)
	{
		if (null == stDeviceList)
		{
			return -1;
		}

		// Choose a device to operate
		int camIndex = -1;
		final Scanner scanner = new Scanner(System.in);

		while (true)
		{
			try
			{
				System.out.print("Please input camera index (-1 to quit):");
				camIndex = scanner.nextInt();
				if ((camIndex >= 0 && camIndex < stDeviceList.size()) || -1 == camIndex)
				{
					break;
				}
				else
				{
					System.out.println("Input error: " + camIndex);
				}
			}
			catch (final Exception e)
			{
				e.printStackTrace();
				camIndex = -1;
				break;
			}
		}
		scanner.close();

		if (-1 == camIndex)
		{
			System.out.println("Bye.");
			return camIndex;
		}

		if (0 <= camIndex && stDeviceList.size() > camIndex)
		{
			if (MvCameraControlDefines.MV_GIGE_DEVICE == stDeviceList.get(camIndex).transportLayerType)
			{
				System.out
					.println(
						"Connect to camera["
								+ camIndex
								+ "]: "
								+ stDeviceList.get(camIndex).gigEInfo.userDefinedName);
			}
			else if (MvCameraControlDefines.MV_USB_DEVICE == stDeviceList.get(camIndex).transportLayerType)
			{
				System.out
					.println(
						"Connect to camera["
								+ camIndex
								+ "]: "
								+ stDeviceList.get(camIndex).usb3VInfo.userDefinedName);
			}
			else
			{
				System.out.println("Device is not supported.");
			}
		}
		else
		{
			System.out.println("Invalid index " + camIndex);
			camIndex = -1;
		}

		return camIndex;
	}

	public static void main(final String[ ] args)
	{
		int nRet = MvCameraControlDefines.MV_OK;
		int camIndex = -1;
		Handle hCamera = null;
		ArrayList<MV_CC_DEVICE_INFO> stDeviceList;

		do
		{
			System.out.println("SDK Version " + MvCameraControl.MV_CC_GetSDKVersion());

			// Enuerate GigE and USB devices
			try
			{
				stDeviceList = MvCameraControl
					.MV_CC_EnumDevices(MvCameraControlDefines.MV_GIGE_DEVICE | MvCameraControlDefines.MV_USB_DEVICE);
				if (0 >= stDeviceList.size())
				{
					System.out.println("No devices found!");
					break;
				}
				int i = 0;
				for (final MV_CC_DEVICE_INFO stDeviceInfo : stDeviceList)
				{
					System.out.println("[camera " + (i++) + "]");
					SaveImage.printDeviceInfo(stDeviceInfo);
				}
			}
			catch (final CameraControlException e)
			{
				System.err.println("Enumrate devices failed!" + e.toString());
				e.printStackTrace();
				break;
			}

			// choose camera
			camIndex = SaveImage.chooseCamera(stDeviceList);
			if (camIndex == -1)
			{
				break;
			}

			// Create handle
			try
			{
				hCamera = MvCameraControl.MV_CC_CreateHandle(stDeviceList.get(camIndex));
			}
			catch (final CameraControlException e)
			{
				System.err.println("Create handle failed!" + e.toString());
				e.printStackTrace();
				hCamera = null;
				break;
			}

			// Open device
			nRet = MvCameraControl.MV_CC_OpenDevice(hCamera);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Connect to camera failed, errcode: [%#x]\n", nRet);
				break;
			}

			// Make sure that trigger mode is off
			nRet = MvCameraControl.MV_CC_SetEnumValueByString(hCamera, "TriggerMode", "Off");
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("SetTriggerMode failed, errcode: [%#x]\n", nRet);
				break;
			}

			// Get payload size
			final MVCC_INTVALUE stParam = new MVCC_INTVALUE();
			nRet = MvCameraControl.MV_CC_GetIntValue(hCamera, "PayloadSize", stParam);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Get PayloadSize fail, errcode: [%#x]\n", nRet);
				break;
			}

			// Start grabbing
			nRet = MvCameraControl.MV_CC_StartGrabbing(hCamera);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Start Grabbing fail, errcode: [%#x]\n", nRet);
				break;
			}

			// Get one frame
			final MV_FRAME_OUT_INFO stImageInfo = new MV_FRAME_OUT_INFO();
			final byte[ ] pData = new byte[ (int) stParam.curValue ];
			nRet = MvCameraControl.MV_CC_GetOneFrameTimeout(hCamera, pData, stImageInfo, 1000);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("GetOneFrameTimeout fail, errcode:[%#x]\n", nRet);
				break;
			}

			System.out.println("GetOneFrame: ");
			SaveImage.printFrameInfo(stImageInfo);
			final int imageLen = stImageInfo.width * stImageInfo.height * 3; // Every RGB pixel takes 3 bytes
			final byte[ ] imageBuffer = new byte[ imageLen ];

			// Call MV_CC_SaveImage to save image as JPEG
			final MV_SAVE_IMAGE_PARAM stSaveParam = new MV_SAVE_IMAGE_PARAM();
			stSaveParam.width = stImageInfo.width; // image width
			stSaveParam.height = stImageInfo.height; // image height
			stSaveParam.data = pData; // image data
			stSaveParam.dataLen = stImageInfo.frameLen; // image data length
			stSaveParam.pixelType = stImageInfo.pixelType; // image pixel format
			stSaveParam.imageBuffer = imageBuffer; // markOutput image buffer
			stSaveParam.imageLen = imageLen; // markOutput image buffer size
			stSaveParam.imageType = MV_SAVE_IAMGE_TYPE.MV_Image_Jpeg; // markOutput image pixel format
			stSaveParam.methodValue = 0; // Interpolation method that converts Bayer format to RGB24.  0-Neareast 1-double linear 2-Hamilton
			stSaveParam.jpgQuality = 90; // JPG endoding quality(50-99]

			nRet = MvCameraControl.MV_CC_SaveImage(hCamera, stSaveParam);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("SaveImage fail, errcode: [%#x]\n", nRet);
				break;
			}

			// Save buffer content to file
			SaveImage.saveDataToFile(imageBuffer, imageLen, "SaveImage.jpeg");

			// Stop grabbing
			nRet = MvCameraControl.MV_CC_StopGrabbing(hCamera);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("StopGrabbing fail, errcode: [%#x]\n", nRet);
				break;
			}
		}
		while (false);

		if (null != hCamera)
		{
			// Destroy handle
			nRet = MvCameraControl.MV_CC_DestroyHandle(hCamera);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("DestroyHandle failed, errcode: [%#x]\n", nRet);
			}
		}
	}

	public static void saveDataToFile(final byte[ ] dataToSave, final int dataSize, final String fileName)
	{
		OutputStream os = null;

		try
		{
			// Create directory
			final File tempFile = new File("dat");
			if (!tempFile.exists())
			{
				tempFile.mkdirs();
			}

			os = new FileOutputStream(tempFile.getPath() + File.separator + fileName);
			os.write(dataToSave, 0, dataSize);
			System.out.println("SaveImage succeed.");
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			// Close file stream
			try
			{
				os.close();
			}
			catch (final IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private static void printDeviceInfo(final MV_CC_DEVICE_INFO stDeviceInfo)
	{
		if (null == stDeviceInfo)
		{
			System.out.println("stDeviceInfo is null");
			return;
		}

		if (stDeviceInfo.transportLayerType == MvCameraControlDefines.MV_GIGE_DEVICE)
		{
			System.out.println("\tCurrentIp:       " + stDeviceInfo.gigEInfo.currentIp);
			System.out.println("\tModel:           " + stDeviceInfo.gigEInfo.modelName);
			System.out.println("\tUserDefinedName: " + stDeviceInfo.gigEInfo.userDefinedName);
		}
		else if (stDeviceInfo.transportLayerType == MvCameraControlDefines.MV_USB_DEVICE)
		{
			System.out.println("\tUserDefinedName: " + stDeviceInfo.usb3VInfo.userDefinedName);
			System.out.println("\tSerial Number:   " + stDeviceInfo.usb3VInfo.serialNumber);
			System.out.println("\tDevice Number:   " + stDeviceInfo.usb3VInfo.deviceNumber);
		}
		else
		{
			System.err.print("Device is not supported! \n");
		}

		System.out
			.println(
				"\tAccessible:      "
						+ MvCameraControl
							.MV_CC_IsDeviceAccessible(stDeviceInfo, MvCameraControlDefines.MV_ACCESS_Exclusive));
		System.out.println("");
	}

	private static void printFrameInfo(final MV_FRAME_OUT_INFO stFrameInfo)
	{
		if (null == stFrameInfo)
		{
			System.err.println("stFrameInfo is null");
			return;
		}

		final StringBuilder frameInfo = new StringBuilder("");
		frameInfo.append(("\tFrameNum[" + stFrameInfo.frameNum + "]"));
		frameInfo.append("\tWidth[" + stFrameInfo.width + "]");
		frameInfo.append("\tHeight[" + stFrameInfo.height + "]");
		frameInfo.append(String.format("\tPixelType[%#x]", stFrameInfo.pixelType.getnValue()));

		System.out.println(frameInfo.toString());
	}
}
