package at.ji.wbchkr;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.jfree.chart.JFreeChart;

import at.ji.utils.measures.statistics.averaging.ActualStatBoolean;
import at.ji.utils.measures.statistics.averaging.SensorStat;
import at.ji.utils.measures.statistics.encoders.DirectionEncoder;
import at.ji.wbchkr.CheckSystem.WorkTask;
import at.ji.wbchkr.configuration.file.YamlConfiguration;
import at.ji.wbchkr.nn.Analizer;
import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class SystemUpdater
{

	public class SessionData
	{
		private boolean edit;
		private boolean stop;//TODO
		private String rollId;
		private String operatorId;
		private File folder;
		private FileWriter out;
		private final long time;

		public AnimatedGifEncoder writer = null;
		public int fps;
		public int write_images;

		public SessionData(final String rollId, final String operatorId, final int count, final boolean stop,
				final boolean edit)
		{
			this.rollId = rollId;
			this.operatorId = operatorId;
			system.getConfiguration().set("session.count", count);
			this.stop = stop;
			this.edit = edit;
			this.folder = new File("systems/" + system.getName() + "/sessions" + "/" + rollId + "/" + operatorId);
			if (!folder.isDirectory())
			{
				folder.mkdirs();
			}
//			try
//			{
//				new File(folder, System.currentTimeMillis() + ".txt").createNewFile();
//			}
//			catch (final Exception e)
//			{
//				throw new RuntimeException(e);
//			}
			fps = system.getConfiguration().getInt("saving.fps");
			time = System.currentTimeMillis();
			try
			{
				final File file = new File(folder, "log.txt");
				if (!file.exists())
				{
					file.createNewFile();
				}

				try
				{
					out = new FileWriter(file, true);
				}
				catch (final Exception e)
				{
					throw new RuntimeException(e);
				}
				out.append("������ " + rollId + " " + operatorId + " " + time + " ��������");
				out.flush();
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}

			if (system.getConfiguration().getBoolean("saving.enabled"))
			{
				newWriter();
			}
			else
			{
				writer = null;
			}
		}

		public void close()
		{
			try
			{
				if (writer != null)
				{
					synchronized (writer)
					{
						writer.finish();
					}
				}
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
			try
			{
				out.close();
			}
			catch (final IOException e)
			{
				e.printStackTrace();
			}
		}

		private void newWriter()
		{
			writer = new AnimatedGifEncoder();
			try
			{
				writer
						.start(
								new FileOutputStream(
										new File(
												folder,
												"gif" + system.getConfiguration().getInt("session.count") + "_"
														+ System.currentTimeMillis() + ".gif"),
										true));
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
			writer.setQuality(100000);
			writer.setFrameRate(fps);
			writer.setRepeat(1);
			writer.setSize(system.getConfiguration().getInt("saving.w"), system.getConfiguration().getInt("saving.h"));
			final BufferedImage img = new BufferedImage(
					system.getConfiguration().getInt("saving.w"),
					system.getConfiguration().getInt("saving.h"),
					BufferedImage.TYPE_BYTE_GRAY);
			img.getGraphics().create();
			img.getGraphics().setColor(Color.WHITE);
			img.getGraphics().setFont(new Font("Calibri", 0, 64));
			img.getGraphics().drawString("START", 100, 100);
			img.getGraphics().drawString("PACKET �" + cnt, 100, 180);
			img.getGraphics().dispose();
			writer.addFrame(img);
		}

	}

	public enum UpdaterInfo
	{
		NO_INFO, NORMAL, SIGNAL, SIGNAL_HARD, OFFSET, STOPPED, STARTED, MOVING, EXCEPTION, INACTIVE
	}

	private static final double directionEncoder_sensorStats_level = 0.6;
	private CheckSystem system;
	private double fpsMultiplier = 1;
	private double directionEncoder_lastStaticValue = Double.NaN;
	private final int directionEncoder_shrink = 2;
	private final DirectionEncoder packetsEncoder = new DirectionEncoder(3);
	private final ActualStatBoolean[ ] directionEncoder_stats = new ActualStatBoolean[ ]
	{ new ActualStatBoolean(100, false), new ActualStatBoolean(100, false) };
	private final long[ ] directionEncoder_stats_lvl = new long[ 0x100 ];
	private final byte directionEncoder_stats_lvl_index = 0;
	private final ActualStatBoolean isMovingCheckerEncoder = new ActualStatBoolean(3000, false);
	private int directionEncoder_lastDirection;
	private final List<boolean[ ]> directionEncoder_debug = new ArrayList<>();
	private final SensorStat[ ] directionEncoder_sensorStats;
	public final List<BufferedImage> lastImages = new ArrayList<>(300);
	public volatile BufferedImage[ ] lastImages_threadSafe = null;
	private final List<double[ ]> runOnceForcedNnProvyazkaData = new ArrayList<>();
	private final List<Integer> runOnceForcedNnProvyazkaData1 = new ArrayList<>();
	private final List<double[ ]> runOnceForcedNnProcessData = new ArrayList<>();//0 result 1 avg mark
	private final List<double[ ]> runOnceForcedNnProcessDataOutputs = new ArrayList<>();
	private int runOnceForcedNnProcessDataOutputWidth = 1;
	private final double[ ] runOnceForcedNnProcessDataMaxValues = new double[ 512 ];
	private byte runOnceForcedNnProcessDataMaxValuesIndex = 0;
	public long stoppedAt = -1;
	public double stoppedAt_level = 0;
	public Analizer.ImageRegistration[ ] runOnceForcedNn_array = null;
	private final Object runOnceForcedNn_lock = new Object();
	private long runOnceForcedNn_array_lastImg;
	private JFreeChart directionEncoder_chart;
	private long directionEncoder_lastDirection_lastFix;
	private double directionEncoder_startValue = 0;
	private final Object sessionLock = new Object();
	private long lastUpdatedImg0;
	private final Set<Integer> markedPackets = new HashSet<>();
	private final Set<String> markedPacketsExplanation = new HashSet<>();
	private final Set<Integer> skipedPackets = new HashSet<>();
	private SessionData sessionData = null;
	private boolean additionalCheckStop = false;
	private int additionalCheckPacketNumber = 0;
	private boolean bypassMode;
	private long startedAt;;
	private long lastMoved;
	private String text = "������";
	private double cnt;
	private long lastStoppedMillis;
	private int lastStoppedPacket;

	private long write_lastWritenPacketTime = 0;
	private volatile boolean stopFlag = false;

	public SystemUpdater(final CheckSystem checkSystem)
	{
		this.system = checkSystem;
		directionEncoder_sensorStats = new SensorStat[ ]
		{ new SensorStat(directionEncoder_sensorStats_level, 1), new SensorStat(directionEncoder_sensorStats_level, 1),
				new SensorStat(directionEncoder_sensorStats_level, 1) };
		updateNewSession();
	}

	public SessionData getSessionData()
	{
		return sessionData;
	}

	public String getText()
	{
		return text;
	}

	public void onError(final String explanation)
	{
		if (system.getListener() != null)
		{
			synchronized (system.getListener())
			{
				system.getListener().event(system, "stop", explanation, null);
				stopFlag = true;
			}
			log("#0x003 ���������� " + explanation);
			System.out.println("stp");
		}
	}

	public void onStop()
	{
		sessionData.close();
	}

	public List<UpdaterInfo> update(final ImageRegistration[ ] regs)
	{
		final List<UpdaterInfo> list = new ArrayList<>();
		boolean marked = false;
		boolean error = false;
		boolean moving = false;
		String explanation = "��������";
		final boolean requiredCheck = false;
		int stoppingPackets = 0;
		int lastPacketNumberPositive = 0;
		if (system.getWorkTask().equals(WorkTask.WORK_COUNT))
		{
			explanation = "�������";
		}
		else if (system.getWorkTask().equals(WorkTask.SLEEP))
		{
			explanation = "��������";
		}
		else if (system.getWorkTask().equals(WorkTask.NONE))
		{
			explanation = "���������";
		}
		final Set<Integer> packets = new HashSet<>();
		cnt = getCount();
		final double cnt0 = cnt;
		for (final ImageRegistration reg : regs)
		{
			if (system.getWorkTask().equals(WorkTask.WORK))
			{
				list.addAll(runOnceForcedNnProcess(reg));
			}
			list.addAll(runOnceForcedNnProcessProvyazka(reg));

//			if (list.contains(UpdaterInfo.MOVING))
//			{
//				System.out.println("Moving");
//			}

			for (final UpdaterInfo info : list)
			{
				lastPacketNumberPositive = Math.max(lastPacketNumberPositive, reg.bagNumber);
				if (info.equals(UpdaterInfo.SIGNAL))
				{
					if (!markedPackets.contains(reg.bagNumber) && !skipedPackets.contains(reg.bagNumber))
					{
						marked = true;
						explanation = "������:" + (int) (stoppedAt_level * 100) + "%";
						markedPackets.add(reg.bagNumber);
						log("#0x001 ����� " + reg.bagNumber + " " + explanation);
						bagstpskp(reg.bagNumber);
						stoppingPackets++;
					}
				}
				else if (info.equals(UpdaterInfo.SIGNAL_HARD))
				{
					if (!markedPackets.contains(reg.bagNumber) && !skipedPackets.contains(reg.bagNumber))
					{
						explanation = "������_�����:" + (int) (stoppedAt_level * 100) + "%";
						markedPackets.add(reg.bagNumber);
						bagstpskp(reg.bagNumber);
						log("#0x001 ����� " + reg.bagNumber + " " + explanation);
						marked = true;
						stoppingPackets++;
					}
				}
				else if (info.equals(UpdaterInfo.EXCEPTION))
				{
					bagmark(reg.bagNumber);
					error = true;
					explanation = "����������:???";
				}
				else if (list.contains(UpdaterInfo.OFFSET))
				{
					bagmark(reg.bagNumber);
					error = true;
					explanation = "�����";
				}

				if (info.equals(UpdaterInfo.MOVING))
				{
					if (system.isSleeping())//TODO synchronize
					{
						system.setWorkTask(WorkTask.WORK, true);
						moving = true;
					}
				}
				else if (info.equals(UpdaterInfo.INACTIVE))
				{
//					if (system.getWorkTask().equals(WorkTask.WORK)) TODO
//					{
//						system.setWorkTask(WorkTask.SLEEP, true);
//					}
				}
			}
		}

		setCount(cnt);

		boolean stop = false;
		if (additionalCheckStop && additionalCheckPacketNumber <= cnt)
		{
			stop = true;
			additionalCheckStop = false;
		}
		else if (350 < System.currentTimeMillis() - lastStoppedMillis)
		{
			for (int i = (int) cnt0; i < (int) cnt; i++)
			{
				if (lastStoppedPacket != i && markedPackets.contains(i))
				{
					System.out.println("������ ��� ��������� " + i);
					stop = true;
					break;
				}
			}
		}

		if (stop)
		{
			lastStoppedMillis = System.currentTimeMillis();
			lastStoppedPacket = (int) cnt;
			onError("updater");
		}

		if (!additionalCheckStop && 0 < stoppingPackets)
		{
			additionalCheckStop = true;
			additionalCheckPacketNumber = lastPacketNumberPositive + 3;
			System.out.println("stpplan");
		}

//		System.out.println(cnt - cnt0);

		if (regs[ 0 ] == null)
		{
			throw new NullPointerException();
		}

		if (skipedPackets.contains(regs[ 0 ].bagNumber) || markedPackets.contains(regs[ 0 ].bagNumber))
		{
			if (explanation.equals("�����"))
			{
				explanation = "�������";
			}
		}

		packets.clear();
		String packetsString = "������ ��� �������� [";
		boolean has = false;
		for (final Integer integer : markedPackets)
		{
			if (cnt <= integer + 3)
			{
				packetsString += (has ? ";" : "") + integer;
				has = true;
			}
		}
		packetsString += "]. ";

		if (packetsString.equals("������ ��� �������� []. "))
		{
			packetsString = "";
		}

		final String text = "" + explanation + ". \r\n" + packetsString + "���������� " + (int) getCount()
				+ (moving ? "+" : "");

		this.text = text;

		system.getThread().write(regs);

		try
		{
			synchronized (sessionData)
			{
				sessionData.out.flush();
			}
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}

		return list;
	}

	public void updateNewSession()//TODO
	{
		markedPackets.clear();

		if (sessionData != null)
		{
			synchronized (sessionData)
			{
				sessionData.close();
			}
		}
		else
		{
			newsdImpl();
			return;
		}

		synchronized (sessionData)
		{
			newsdImpl();
		}
	}

	public void write(final BufferedImage image, final long time)
	{
		final AnimatedGifEncoder writer = system.getSessionManager().getSessionData().writer;
		if (writer != null)
		{
			synchronized (writer)
			{
				if (100 < getSessionData().write_images)
				{
					getSessionData().write_images = 0;
					try
					{
						getSessionData().writer.finish();
						getSessionData().newWriter();
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
				}

				if ((1000 / sessionData.fps) <= time - write_lastWritenPacketTime)
				{
//					writer.addFrame(image);

					getSessionData().write_images++;
					write_lastWritenPacketTime = time;

					try
					{
						ImageIO
								.write(
										image,
										"jpg",
										new File(getSessionData().folder, "img" + (int) cnt + "_" + time + ".jpg"));
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}

					if (stopFlag)
					{
//						final BufferedImage img = new BufferedImage(
//								image.getWidth(),
//								image.getHeight(),
//								image.getType());
//						img.getGraphics().create();
//						img.getGraphics().setColor(Color.WHITE);
//						img.getGraphics().setFont(new Font("Calibri", 0, 64));
//						img.getGraphics().drawString("STOP", 100, 100);
//						img.getGraphics().drawString("PACKET �" + cnt, 180, 180);
//						img.getGraphics().dispose();
//						writer.addFrame(img);
//						stopFlag = false;
					}
				}
			}
		}
	}

	public void write(final ImageRegistration reg)
	{
		write(reg.image, reg.time);
	}

	private void bagmark(final int bagNumber)
	{
		markedPackets.add(bagNumber);
//		System.out.println("bagstp " + bagNumber);
	}

	private void bagstpskp(final int bagNumber)
	{
		bagmark(bagNumber);
//		System.out.println("bagskp " + bagNumber);
		skipedPackets.add(bagNumber);
	}

	private double getCount()
	{
		synchronized (system.getConfiguration())
		{
			return system.getConfiguration().getDefaults().getDouble("session.count");
		}
	}

	private void log(final String log)
	{
		try
		{
			synchronized (sessionData.out)
			{
				sessionData.out.write(log);
				sessionData.out.write("\r\n");
			}
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
	}

	private void newsdImpl()
	{
		sessionData = new SessionData(
				system.getConfiguration().getString("session.rollId"),
				system.getConfiguration().getString("session.operatorId"),
				system.getConfiguration().getInt("session.count"),
				system.getConfiguration().getBoolean("session.stopOnWebEnd"),
				system.getConfiguration().getBoolean("session.allowCountEdit"));
	}

	private List<UpdaterInfo> runOnceForcedNnProcess(final ImageRegistration e)
	{
		final List<UpdaterInfo> list = new ArrayList<>();
		final NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(4);
		numberFormat.setMinimumFractionDigits(4);
		final YamlConfiguration settings = system.getConfiguration();
		final double settings_maxTop = settings.getDouble("nn.level.max");
		final double settings_maxAvg = settings.getDouble("nn.level.average");
		final int settings_imagesCount = settings.getInt("nn.images");
		final double[ ] result = e.getResult();
		final int nn_statistics_imagesCount = (int) (settings_imagesCount * fpsMultiplier);
		while (!runOnceForcedNnProcessData.isEmpty() && nn_statistics_imagesCount <= runOnceForcedNnProcessData.size())
		{
			runOnceForcedNnProcessData.remove(0);
			runOnceForcedNnProcessDataOutputs.remove(0);
		}
		if (result == null)
		{
			if (e.hasOffset)
			{
				list.add(UpdaterInfo.OFFSET);
				return list;
			}
			System.err.println("No result");
			list.add(UpdaterInfo.EXCEPTION);
		}
		runOnceForcedNnProcessData.add(result);
		runOnceForcedNnProcessDataOutputs.add(e.getOutput());
		runOnceForcedNnProcessDataOutputWidth = e.getOutputWidth();

		final double[ ] avg = new double[ 2 ];
		if (runOnceForcedNnProcessData.size() == nn_statistics_imagesCount)
		{
			for (final double[ ] data3 : runOnceForcedNnProcessData)
			{
				if (data3 == null)
				{
					System.err.println("!");
				}
				avg[ 0 ] += data3[ 0 ];
				avg[ 1 ] += data3[ 1 ];
			}
			avg[ 0 ] /= Double.valueOf(runOnceForcedNnProcessData.size());
			avg[ 1 ] /= Double.valueOf(runOnceForcedNnProcessData.size());
		}
		String output = "";

		runOnceForcedNnProcessDataMaxValues[ (runOnceForcedNnProcessDataMaxValuesIndex & 0xFF) * 2 ] = avg[ 0 ];
		runOnceForcedNnProcessDataMaxValues[ (runOnceForcedNnProcessDataMaxValuesIndex & 0xFF) * 2 + 1 ] = avg[ 1 ];
		runOnceForcedNnProcessDataMaxValuesIndex++;

		if (settings_maxAvg != 0 && settings_maxAvg < avg[ 1 ])
		{
//			System.out.println("Case avg " + numberFormat.format(avg[ 1 ]));
			stoppedAt_level = avg[ 1 ];
			list.add(UpdaterInfo.SIGNAL);
			output = "HOLE avg " + numberFormat.format(avg[ 1 ]);
		}
		else if (settings_maxTop != 0 && settings_maxTop < avg[ 0 ])
		{
//			System.out.println("Case max " + numberFormat.format(avg[ 0 ]));
			stoppedAt_level = avg[ 0 ];
			list.add(UpdaterInfo.SIGNAL_HARD);
			output = "RABBIT max " + numberFormat.format(avg[ 0 ]);
		}
		else if (e.hasOffset)
		{
			list.add(UpdaterInfo.OFFSET);
			output = "OFFSET";
		}
		try
		{
			sessionData.out
					.write(
							"�����/�����/����������/���������/��.��������� " + cnt + " " + " " + output + " "
									+ Arrays.toString(result) + " " + Arrays.toString(avg) + "\r\n");
		}
		catch (final Exception e1)
		{
			e1.printStackTrace();
		}

		return list;
	}

	private List<UpdaterInfo> runOnceForcedNnProcessProvyazka(final ImageRegistration e)
	{
		final List<UpdaterInfo> list = new ArrayList<>();
		final YamlConfiguration settings = system.getConfiguration();
		final double settings_counterLevel = settings.getDouble("provyazka.level");
		final double count = settings.getDouble("provyazka.images");
		double[ ] prs = Arrays
				.copyOfRange(
						e.provyazkaResult,
						directionEncoder_shrink,
						e.provyazkaResult.length - directionEncoder_shrink);
		final double[ ] provyazkaResultShrinked_ = new double[ ]
		{ prs[ 0 ], prs[ prs.length / 2 - 1 ], prs[ prs.length - 1 ] };
		prs = provyazkaResultShrinked_;
		runOnceForcedNnProvyazkaData.add(prs);
		if (20 <= runOnceForcedNnProvyazkaData.size())
		{
			runOnceForcedNnProvyazkaData.remove(0);
		}
		final double[ ] avg = new double[ prs.length ];
		{
			int avgcnt = 0;
			final double counter_imagesCount = count;
			final double set = Math.max(1, counter_imagesCount);
//			setCount(set);

			for (int i = 0; i < Math.min(counter_imagesCount, runOnceForcedNnProvyazkaData.size()); i++)
			{
				final double[ ] ds = runOnceForcedNnProvyazkaData.get(runOnceForcedNnProvyazkaData.size() - 1 - i);
				for (int j = 0; j < ds.length; j++)
				{
					avg[ j ] += ds[ j ];
				}
				avgcnt++;
			}

			for (int i = 0; i < avg.length; i++)
			{
				avg[ i ] /= avgcnt;
			}
		}
		try
		{
//			System.out.println("Probabilities " + Arrays.toString(avg));
//			if (packetsEncoder.getLastActivation() != -1)

			packetsEncoder.update(avg, settings_counterLevel);
		}
		catch (final Exception e2)
		{
			e2.printStackTrace();
		}

		runOnceForcedNnProvyazkaData1.add(packetsEncoder.getLastActivation());
		if (40 <= runOnceForcedNnProvyazkaData1.size())
		{
			runOnceForcedNnProvyazkaData1.remove(0);
		}
		boolean isMoving = false;

		final int direction = packetsEncoder.getDirection();
//		if (direction != 0)
//		{
//			System.out.println("Direction" + " " + direction);
//		}
		if (packetsEncoder.getCause() == null)
		{
			if (direction != 0 && direction == directionEncoder_lastDirection)
			{
				isMoving = true;
			}
			if (direction != 0)
			{
				directionEncoder_lastDirection = direction;
			}
			final double rotation = packetsEncoder.getRotation();
//			System.out.println(rotation);
			if (rotation != 0)
			{
				cnt += rotation;
			}
			try
			{
				{
					final int v0 = (int) (avg[ 0 ] * 100);
					final int v1 = (int) (avg[ 1 ] * 100);
					final int v2 = (int) (avg[ 2 ] * 100);
					sessionData.out
							.write(
									"time/values " + e.time + " " + v0 + " " + v1 + " " + v2 + " "
											+ packetsEncoder.getDirection() + " " + cnt + "\r\n");
				}
			}
			catch (final IOException e1)
			{
				e1.printStackTrace();
			}
		}

		if (isMoving)
		{
			list.add(UpdaterInfo.MOVING);
		}
		else
		{
			list.add(UpdaterInfo.INACTIVE);
		}
		e.bagNumber = (int) getCount();
		return list;
	}

	private void setCount(final double set)
	{
//		System.out.println("Setting count to " + set);
		synchronized (system.getConfiguration())
		{
			system.getConfiguration().getDefaults().set("session.count", set);
		}
	}

}
