package at.ji.wbchkr.configuration.file;

/**
 * Utils for casting number types to other number types
 */
public final class NumberConversions
{
	private NumberConversions()
	{}
	
	public static int ceil(final double num)
	{
		final int floor = (int) num;
		return floor == num ? floor : floor + (int) (~Double.doubleToRawLongBits(num) >>> 63);
	}
	
	public static void checkFinite(final double d, final String message)
	{
		if (!NumberConversions.isFinite(d))
		{
			throw new IllegalArgumentException(message);
		}
	}
	
	public static void checkFinite(final float d, final String message)
	{
		if (!NumberConversions.isFinite(d))
		{
			throw new IllegalArgumentException(message);
		}
	}
	
	public static int floor(final double num)
	{
		final int floor = (int) num;
		return floor == num ? floor : floor - (int) (Double.doubleToRawLongBits(num) >>> 63);
	}
	
	public static boolean isFinite(final double d)
	{
		return Math.abs(d) <= Double.MAX_VALUE;
	}
	
	public static boolean isFinite(final float f)
	{
		return Math.abs(f) <= Float.MAX_VALUE;
	}
	
	public static int round(final double num)
	{
		return NumberConversions.floor(num + 0.5d);
	}
	
	public static double square(final double num)
	{
		return num * num;
	}
	
	public static byte toByte(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).byteValue();
		}
		
		try
		{
			return Byte.parseByte(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
	
	public static double toDouble(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).doubleValue();
		}
		
		try
		{
			return Double.parseDouble(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
	
	public static float toFloat(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).floatValue();
		}
		
		try
		{
			return Float.parseFloat(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
	
	public static int toInt(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).intValue();
		}
		
		try
		{
			return Integer.parseInt(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
	
	public static long toLong(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).longValue();
		}
		
		try
		{
			return Long.parseLong(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
	
	public static short toShort(final Object object)
	{
		if (object instanceof Number)
		{
			return ((Number) object).shortValue();
		}
		
		try
		{
			return Short.parseShort(object.toString());
		}
		catch (final NumberFormatException e)
		{}
		catch (final NullPointerException e)
		{}
		return 0;
	}
}
