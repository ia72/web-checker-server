package at.ji.wbchkr.configuration.file;

import java.util.LinkedHashMap;
import java.util.Map;

import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.representer.Representer;

import at.ji.wbchkr.configuration.ConfigurationSection;
import at.ji.wbchkr.configuration.serialization.ConfigurationSerializable;
import at.ji.wbchkr.configuration.serialization.ConfigurationSerialization;

public class YamlRepresenter extends Representer
{

	private class RepresentConfigurationSection extends RepresentMap
	{
		@Override
		public Node representData(final Object data)
		{
			return super.representData(((ConfigurationSection) data).getValues(false));
		}
	}

	private class RepresentConfigurationSerializable extends RepresentMap
	{
		@Override
		public Node representData(final Object data)
		{
			final ConfigurationSerializable serializable = (ConfigurationSerializable) data;
			final Map<String, Object> values = new LinkedHashMap<>();
			values
				.put(
					ConfigurationSerialization.SERIALIZED_TYPE_KEY,
					ConfigurationSerialization.getAlias(serializable.getClass()));
			values.putAll(serializable.serialize());

			return super.representData(values);
		}
	}

	public YamlRepresenter()
	{
		multiRepresenters.put(ConfigurationSection.class, new RepresentConfigurationSection());
		multiRepresenters.put(ConfigurationSerializable.class, new RepresentConfigurationSerializable());
	}
}
