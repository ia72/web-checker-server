package at.ji.wbchkr;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

public class ControlFrame extends JFrame
{

	private JPanel contentPane;

	/**
	 * Create the frame.
	 * @param analizationThread 
	 */
	public ControlFrame(final GlobalThread analizationThread)
	{
		super("�������� ������");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(0, 0, 640, 200);

		final JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		final CheckSystem[ ] systems = analizationThread.getSystems();
		for (int i = 0; i < systems.length; i++)
		{
			tabbedPane.addTab(systems[ i ].getName(), getTab(systems[ i ]));
		}
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		addWindowListener(new WindowListener()
		{

			@Override
			public void windowActivated(final WindowEvent e)
			{
			}

			@Override
			public void windowClosed(final WindowEvent e)
			{
			}

			@Override
			public void windowClosing(final WindowEvent e)
			{
				analizationThread.requestExit();
			}

			@Override
			public void windowDeactivated(final WindowEvent e)
			{

			}

			@Override
			public void windowDeiconified(final WindowEvent e)
			{

			}

			@Override
			public void windowIconified(final WindowEvent e)
			{

			}

			@Override
			public void windowOpened(final WindowEvent e)
			{

			}
		});
	}

	private Component getTab(final CheckSystem checkSystem)
	{
		return new SystemTab(checkSystem);
	}

	/**
	 * Launch the application.
	 */
	public static void main(final String[ ] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					final ControlFrame frame = new ControlFrame(null);
					frame.setVisible(true);
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

}
