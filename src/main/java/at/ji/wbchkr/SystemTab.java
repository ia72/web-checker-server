package at.ji.wbchkr;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import at.ji.wbchkr.CheckSystem.WorkTask;

public class SystemTab extends JScrollPane
{
	private CheckSystem system;
	private JTree tree;

	/**
	 * Create the frame.
	 * @param checkSystem 
	 */
	public SystemTab(final CheckSystem checkSystem)
	{
		this.system = checkSystem;
		setBounds(100, 100, 450, 300);
		setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
		setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);

		tree = new JTree();
		tree.setName("�����");
		tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("�����")
		{
			{
				DefaultMutableTreeNode node;
				node = new DefaultMutableTreeNode("��������");
				final Set<Entry<String, Runnable>> entrySet = getOperations().entrySet();
				for (final Entry<String, Runnable> e : entrySet)
				{
					final DefaultMutableTreeNode node2 = new DefaultMutableTreeNode(e.getKey());
					node.add(node2);
					node2.setUserObject(e.getValue());
				}
				add(node);
				node = new DefaultMutableTreeNode("������");
				node.add(new DefaultMutableTreeNode("�����"));
				node.add(new DefaultMutableTreeNode("������"));
				node.add(new DefaultMutableTreeNode("�����"));
				node.add(new DefaultMutableTreeNode("�����"));
				add(node);
				node = new DefaultMutableTreeNode("���������");
				node.add(new DefaultMutableTreeNode("1"));
				node.add(new DefaultMutableTreeNode("2"));
				node.add(new DefaultMutableTreeNode("3"));
				node.add(new DefaultMutableTreeNode("4"));
				add(node);
			}
		}));

		final JPopupMenu popup = new JPopupMenu();
		tree.addMouseListener(new MouseListener()
		{

			@Override
			public void mouseClicked(final MouseEvent e)
			{
				treeAtionPerformed();
			}

			@Override
			public void mouseEntered(final MouseEvent e)
			{
			}

			@Override
			public void mouseExited(final MouseEvent e)
			{
			}

			@Override
			public void mousePressed(final MouseEvent e)
			{
			}

			@Override
			public void mouseReleased(final MouseEvent e)
			{
				java.awt.EventQueue.invokeLater(new Runnable()
				{

					@Override
					public void run()
					{
						tree.setSelectionPath(null);
					}
				});
			}
		});
		tree.setComponentPopupMenu(popup);
		setViewportView(tree);
	}

	public Map<String, Runnable> getOperations()
	{
		final Map<String, Runnable> map = new TreeMap<>();
		map.put("�����: ���� ����� � ��������", new Runnable()
		{
			@Override
			public void run()
			{
				system.setWorkTask(WorkTask.WORK, true);
			}

			@Override
			public String toString()
			{
				return "�����: ���� ����� � ��������";
			}
		});

		map.put("�����: ���� �����", new Runnable()
		{
			@Override
			public void run()
			{
				system.setWorkTask(WorkTask.WORK_COUNT, true);
			}

			@Override
			public String toString()
			{
				return "�����: ���� �����";
			}
		});

		map.put("�����: ���", new Runnable()
		{
			@Override
			public void run()
			{
				system.setWorkTask(WorkTask.SLEEP, true);
			}

			@Override
			public String toString()
			{
				return "�����: ���";
			}
		});

		map.put("�����: ��������", new Runnable()
		{
			@Override
			public void run()
			{
				system.setWorkTask(WorkTask.NONE, true);
				System.out.println(WorkTask.NONE);
			}

			@Override
			public String toString()
			{
				return "�����: ��������";
			}
		});

		map.put("�������", new Runnable()
		{
			@Override
			public void run()
			{
				if (system.getConfiguration().contains("debug") && system.getConfiguration().getBoolean("debug"))
				{
					system.getConfiguration().set("debug", false);
				}
				else
				{
					system.getConfiguration().set("debug", true);
				}
			}

			@Override
			public String toString()
			{
				return "�������";
			}
		});

//		map.put("����� ��������", new Runnable()
//		{
//			@Override
//			public void run()
//			{
//				try
//				{
//					synchronized (system.getConfiguration())
//					{
//						system.getConfiguration().getDefaults().set("session.count", 0);
//					}
//				}
//				catch (final Exception e)
//				{
//					e.printStackTrace();
//				}
//			}
//
//			@Override
//			public String toString()
//			{
//				return "����� ��������";
//			}
//		});

		map.put("������ ������", new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					final SessionFrame sessionFrame = new SessionFrame(system);
					sessionFrame.setVisible(true);
					sessionFrame.requestFocus();
				}
				catch (final Exception e)
				{
					e.printStackTrace();
				}
			}

			@Override
			public String toString()
			{
				return "������ ������";
			}
		});

		return map;
	}

	protected void treeAtionPerformed()
	{
		if (tree.getSelectionCount() == 0)
		{
			return;
		}
		final Object[ ] path = tree.getSelectionPath().getPath();
		if (path.length != 3)
		{
			return;
		}

		final DefaultMutableTreeNode node1 = (DefaultMutableTreeNode) path[ 2 ];
		final Object userObject = node1.getUserObject();
		if (userObject != null && userObject instanceof Runnable)
		{
			((Runnable) userObject).run();
		}
	}
}
