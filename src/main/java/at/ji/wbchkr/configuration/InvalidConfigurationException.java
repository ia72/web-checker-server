package at.ji.wbchkr.configuration;

/**
 * Exception thrown when attempting to load an invalid {@link Configuration}
 */
@SuppressWarnings("serial")
public class InvalidConfigurationException extends Exception
{
	private final Configuration configuration;
	
	/**
	 * Creates a new instance of InvalidConfigurationException without a
	 * message or cause.
	 * @param configuration TODO
	 */
	public InvalidConfigurationException(final Configuration configuration)
	{
		this.configuration = configuration;
	}

	/**
	 * Constructs an instance of InvalidConfigurationException with the
	 * specified message.
	 *
	 * @param msg The details of the exception.
	 */
	public InvalidConfigurationException(final String msg)
	{
		super(msg);
		configuration = null;//TODO
	}

	/**
	 * Constructs an instance of InvalidConfigurationException with the
	 * specified message and cause.
	 *
	 * @param cause The cause of the exception.
	 * @param msg The details of the exception.
	 */
	public InvalidConfigurationException(final String msg, final Throwable cause)
	{
		super(msg, cause);
		configuration = null;//TODO
	}

	/**
	 * Constructs an instance of InvalidConfigurationException with the
	 * specified cause.
	 *
	 * @param cause The cause of the exception.
	 */
	public InvalidConfigurationException(final Throwable cause)
	{
		super(cause);
		configuration = null;//TODO
	}

	public Configuration getConfiguration()
	{
		return configuration;
	}
}
