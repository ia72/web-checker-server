package at.ji.wbchkr;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import at.ji.wbchkr.configuration.file.YamlConfiguration;

public class SystemsInitializator
{
	public static YamlConfiguration getConfigRaw(final File file)
	{
		final YamlConfiguration config;
		if (!file.exists())
		{
			final YamlConfiguration config_ = SystemsInitializator.createDefaultConfig();
			config_.set("name", file.getParentFile().getName());
			config_.setConfigurationFile(file);
			try
			{
				config_.save();
			}
			catch (final Exception e)
			{
				throw new RuntimeException("�� ������� ��������� ����", e);
			}
		}
		config = YamlConfiguration.loadConfiguration(file);
		return config;
	}

	@Deprecated
	public static CheckSystem load(final File folder)
	{
		final YamlConfiguration config;
		final File file = new File(folder, "config.yml");
		config = SystemsInitializator.getConfigRaw(file);

		final YamlConfiguration systemConfig = new YamlConfiguration();
		systemConfig.setDefaults(config);
		final CheckSystem system = new CheckSystem(systemConfig);
		if (!system.getConfiguration().getString("name").equals(file.getParentFile().getName()))
		{
			throw new IllegalStateException("����������� ������� ������� " + file.getParentFile().getName());
		}
		return system;
	}

	public static CheckSystem[ ] loadAndTryInitialize(final File[ ] folders)
	{
		final CheckSystem[ ] array = new CheckSystem[ folders.length ];
		for (int i = 0; i < folders.length; i++)
		{
			try
			{
				array[ i ] = SystemsInitializator.load(folders[ i ]);
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
		final List<Object> list = new ArrayList<>();
		for (int i = 0; i < folders.length; i++)
		{
			if (array[ i ] == null)
			{
				continue;
			}
			try
			{
				list.add(array[ i ]);
				array[ i ].initialize();
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
		return list.toArray(new CheckSystem[ list.size() ]);
	}

	private static YamlConfiguration createDefaultConfig()
	{
		final YamlConfiguration config = new YamlConfiguration();
		config.set("name", "example");
		config.set("status", "WORK");
		config.set("session.operatorId", "UNKNOWN");
		config.set("session.rollId", "UNKNOWN");
		config.set("session.count", 0);
		config.set("session.initialCount", 0);
		config.set("session.stopOnWebEnd", true);
		config.set("session.allowCountEdit", true);
		config.set("image.protocol", "mvs");
		config.set("image.id", "123456 ��� 00G09034860 !!!!!!!!!!!!");
		config.set("image.offset.horizontal", 0);
		config.set("image.offset.vertical", 40 / 2);
		config.set("image.binning.horizontal", 2);
		config.set("image.binning.vertical", 2);
		config.set("image.size.x", 640);
		config.set("image.size.y", 360);
		config.set("image.exposition", 750);
		config.set("image.targetBrightness", 225);
		config.set("nn.fps", 60);
		config.set("nn.images", 16);
		config.set("nn.level.average", 0.0025);
		config.set("nn.level.max", 0.8);
		config.set("nn.level.output", 0.8);
		config.set("nn.minibatchSize", 10);
		config.set("nn.agentFile", "��������-������(�����_agents).nn");
		config.set("nn.dropout", "RESERVED");//TODO
		config.set("nn.edgeFix.padding", 30);
		config.set("nn.edgeFix.minValue", 40);
		config.set("nn.edgeFix.maxEdgeDifference", 40);
		config.set("nn.statistics.maxTopValueLevel", "RESERVED");//TODO
		config.set("control.stop.delay.0", 350);
		config.set("control.stop.delay.1", 250);
		config.set("control.stop.delay.2", 150);
		config.set("control.stop.delay.3", 0);
		config.set("control.stop.delay.4", 0);
		config.set("padding.levelNeightbor", 0.3);
		config.set("padding.levelGlobal", 0.4);
		config.set("provyazka.images", 4);
		config.set("provyazka.level", 0.4);
		config.set("provyazka.encoderSteps", 9);
		config.set("provyazka.encoderCut", 2);
		config.set("provyazka.agentFile", "��������-������.nn");
		config.set("serial.commPort", "COM3 ��� COM4 ��� EMPTY !!!!!!!!!!!!!!");
		config.set("serial.id", 0);
		config.set("serial.baudRate", 9600);
		config.set("serial.timeOut", 2000);
		config.set("ui.x", 0);
		config.set("ui.y", 0);
		config.set("ui.w", 640);
		config.set("ui.h", 480);
		config.set("saving.enabled", true);
		config.set("saving.fps", 10);
		config.set("saving.quality", 0.4);
		config.set("saving.id", "session000");
		config.set("saving.w", 640);
		config.set("saving.h", 360);
		return config;
	}
}
