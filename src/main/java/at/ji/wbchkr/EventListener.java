package at.ji.wbchkr;

import java.awt.image.BufferedImage;

public abstract class EventListener
{
	/**
	 *
	 * @param system
	 * @param id init, deinit, tick, count, move (start, stop), stop (deffect, deffect_hard, offset, error)
	 * @param data
	 * @param img
	 */
	public abstract void event(CheckSystem system, String id, String data, BufferedImage img);
	
	public abstract boolean isFine();
}
