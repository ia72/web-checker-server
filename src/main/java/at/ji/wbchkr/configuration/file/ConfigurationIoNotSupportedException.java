package at.ji.wbchkr.configuration.file;

import javax.naming.OperationNotSupportedException;

import at.ji.wbchkr.configuration.Configuration;

public class ConfigurationIoNotSupportedException extends OperationNotSupportedException
{
	/**
	 *
	 */
	private static final long serialVersionUID = 693647161535036652L;
	
	private final Configuration configuration;

	private final boolean isSaveAttempt;

	public ConfigurationIoNotSupportedException(final Configuration configuration, final boolean isSaveAttempt)
	{
		super();
		this.configuration = configuration;
		this.isSaveAttempt = isSaveAttempt;
	}
	public ConfigurationIoNotSupportedException(final String explanation, final Configuration configuration,
		final boolean isSaveAttempt)
	{
		super(explanation);
		this.configuration = configuration;
		this.isSaveAttempt = isSaveAttempt;
	}


	public Configuration getConfiguration()
	{
		return configuration;
	}
	
	public boolean isSaveAttempt()
	{
		return isSaveAttempt;
	}
}
