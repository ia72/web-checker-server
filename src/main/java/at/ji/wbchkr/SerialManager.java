package at.ji.wbchkr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import com.fazecast.jSerialComm.SerialPort;

import at.ji.utils.io.protocols.sssstdp.SssstdpMessage;

public class SerialManager
{
	private static final int CONFIG_SPAM_TIMEOUT = 15;
	private static final int CONFIG_SPAM_INTERVAL = 20;
	private static final int MSG_CMD_SET_LED = 2;
	private static final int MSG_EMPTY = 6;
	private static final int MSG_NO_SIGNAL = 4;
	private static final int MSG_CMD_STOP = 1;
	private final Object lock = new Object();
	private final CheckSystem system;

	private boolean enabledPort = false;

	private long debug_lastUpdated = 0;

	private volatile SerialPort serialPort;

	private int oldStatus = -1;

	private final Pattern pattern = Pattern.compile("st\\d++_++");

	private volatile boolean checkRequest = false;

	private boolean fullInit = false;

//	private SerialPortListenerImpl listener;
	private long lastTr;
	private volatile boolean stopRequest = false;
	private boolean connected = false;
	private boolean debug = false;

	public SerialManager(final CheckSystem system)
	{
		super();
		this.system = system;
	}

	public void check()
	{
		if (fullInit)
		{
			checkRequest = true;
		}
	}

	public void close()
	{
		System.out.println("Closing comm port " + serialPort.getSystemPortName());
		synchronized (lock)
		{
			if (serialPort != null)
			{
//				serialPort.setBreak();
//				try
//				{
//					Thread.sleep(500);
//				}
//				catch (final Exception e)
//				{
//					throw new RuntimeException(e);
//				}
//				serialPort.clearBreak();
				serialPort.removeDataListener();
				serialPort.closePort();
				unregister();
				enabledPort = false;
			}
		}
		try
		{
			Thread.sleep(1500);
		}
		catch (final Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("rawtypes")
	public synchronized void initialize()
	{
		if (enabledPort)
		{
			close();
		}
		{
			final com.fazecast.jSerialComm.SerialPort[ ] commPorts = com.fazecast.jSerialComm.SerialPort.getCommPorts();
			final List<String> list = new ArrayList<>();
			for (final com.fazecast.jSerialComm.SerialPort i : commPorts)
			{
				list.add(i.getSystemPortName());
			}
			if (list.isEmpty())
			{
				throw new IllegalArgumentException("No com ports detected");
			}
		}
		com.fazecast.jSerialComm.SerialPort serialPort = null;
		{
			{
				final String id = system.getConfiguration().getString("serial.commPort");
				try
				{
					System.out.println("����� " + id);
					final SerialPort commPort = SerialPort.getCommPort(id);
					if (commPort == null || !commPort.openPort())
					{
						System.out.println("Opened reserved port " + commPort.getSystemPortName());
					}
					commPort.closePort();
					serialPort = commPort;
				}
				catch (final Exception e)
				{
					throw new RuntimeException("�� ������� ������� ����. ���� ����� ��� �����������?");
				}
			}
		}
		if (serialPort == null)
		{
			System.out.println("Could not find COM port.");
			throw new IllegalArgumentException("Com port is not initialized");
		}
		else
		{
			System.out.println("Found  port " + serialPort.getSystemPortName());
		}
		try
		{
			// open serial port, and use class name for the appName.
			// set port parameters
			this.serialPort = serialPort;

			if (serialPort.isOpen())
			{
				throw new IOException("���� �����");
			}

			// open the streams
			System.out.println("Openning  port " + serialPort.getSystemPortName());

			if (!serialPort.openPort() || !serialPort.isOpen())
			{
				throw new IllegalStateException("���� �� ����������� " + serialPort.getSystemPortName());
			}
//			listener = new SerialPortListenerImpl(3);
//			serialPort.addDataListener(listener);
			config();
			System.out.println("Opened  port " + serialPort.getSystemPortName() + " " + serialPort.isOpen());
			final Runnable run = new Runnable()
			{

				@Override
				public void run()
				{
					invoke();

				}
			};
			final Thread thread = new Thread(run, "Com port  communication thread");
			final Thread threadComStop = new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
					try
					{
						while (enabledPort)
						{
							Thread.sleep(5);
							if (stopRequest)
							{
								stopRequest = false;
								sendStopSignal();
							}
						}
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}
					System.out.println("Stopping stopCom comm port thread");
					try
					{
						Thread.sleep(10000);
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}
					System.exit(1);
				}
			}, "Com port stop communication thread");
			enabledPort = true;
			thread.setDaemon(true);
			thread.setName("Comm thread of " + system.getName());
			threadComStop.setDaemon(true);
			threadComStop.setName("Comm stopping thread of " + system.getName());

			while (true)
			{
				boolean checkConnectionImpl;
				try
				{
					checkConnectionImpl = checkConnectionImpl();
					connected = checkConnectionImpl;
				}
				catch (final Exception e1)
				{
					e1.printStackTrace();
					checkConnectionImpl = false;
				}

				if (checkConnectionImpl)
				{
					System.out.println("Connection has been checked");
					break;
				}
				else
				{
					System.err
							.println(
									"############################" + System.lineSeparator()
											+ "������� ���������� �����. ���� ��� ��������� �����������, ��������� � �������� COM ������������� � ����� � ������ � 5 ������.");
					try
					{
						reopen(serialPort);
					}
					catch (final Exception e)
					{
						System.err.println("�� ������ ����������� ����");
					}
				}
			}
			thread.start();
			threadComStop.start();
			fullInit = true;
		}
		catch (final Exception e)
		{
			throw new RuntimeException("�� ������� ��������� SM", e);
		}
		register();
		System.out.println("Connected  port " + serialPort.getDescriptivePortName());
	}

	public void invoke()
	{
		Thread.currentThread().setPriority(Thread.NORM_PRIORITY);
		final int i = 0;
		int failcnt = 0;
		while (enabledPort)
		{
			if (!serialPort.isOpen())
			{
				try
				{
					Thread.sleep(50);
				}
				catch (final Exception e)
				{
					throw new RuntimeException(e);
				}
			}
			try
			{
				if (system.getStatus().led)
				{
					tr(SerialManager.MSG_CMD_SET_LED, "thr");
				}
				else
				{
					tr(SerialManager.MSG_EMPTY, "thr");
				}
			}
			catch (final Exception e1)
			{
				checkRequest = true;
				e1.printStackTrace();
			}

			try
			{
				final int status = getStatus();
				if (oldStatus != status)
				{
					System.err.println("Com port status changed to " + status);
					oldStatus = status;
				}
				getStatus();
//				System.out
//					.println(
//						status
//								+ ", "
//								+ serialPort.getDeviceReadBufferSize()
//								+ ", "
//								+ serialPort.getDeviceWriteBufferSize());
				if (fullInit && !serialPort.isOpen())
				{
					try
					{
						reopen(serialPort);
					}
					catch (final Exception e)
					{
						System.err.println("SMTHR case 2 " + e.getMessage());
					}
				}
				if (serialPort.isOpen())
				{
					if (checkRequest)
					{
						synchronized (lock)
						{
							final int msgIndex = 5 <= failcnt ? SerialManager.MSG_NO_SIGNAL : SerialManager.MSG_EMPTY;
//							listener.getMessage("threadreset");
							final String read = tr(msgIndex, "check");
							final boolean assert1 = assert1(read);

							checkRequest = false;
							if (!assert1)
							{

								failcnt++;
								checkRequest = true;
							}
							else
							{
								failcnt = 0;
							}

							if (10 < failcnt)
							{
								System.err.println("Stopping due to connection issues");
								connected = false;
								try
								{
									reopen(serialPort);
								}
								catch (final Exception e)
								{
									System.err.println("SMTHR " + e.getMessage());
								}
							}
						}
					}
				}
			}
			catch (final Exception e)
			{
				e.printStackTrace();
				system.deinitialize();
			}
			try
			{
				Thread.sleep(8);
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
		}
		System.out.println("Stopping comm port thread");
	}

	public boolean isConnected()
	{
		return connected;
	}

	public boolean isFine()
	{
		return isConnected();
	}

	public void sendMessage(final int index)
	{
		final SssstdpMessage orNull = SssstdpMessage.getOrNull(index);
		if (orNull == null)
		{
			throw new IllegalArgumentException("Tried to send msg with index " + index);
		}

		try
		{
			synchronized (lock)
			{

				final byte myByte = orNull.value();
				final byte[ ] buffer = new byte[ ]
				{ myByte, (byte) ~myByte };
				if (index != 2 || Math.random() < 0.01)
				{
					System.out.println("Serial data " + Arrays.toString(buffer));
				}
				final int writeBytes = serialPort.writeBytes(buffer, 2L);
//				System.out.println("Write status " + getStatus());
				System.out.println("Writen " + writeBytes);
			}

			if (5000 < System.currentTimeMillis() - (debug_lastUpdated))
			{
				debug_lastUpdated = System.currentTimeMillis();
//				debug = "";
			}

//			System.out.println("Sent " + (myByte & 0xFF));
		}
		catch (final Exception e)
		{
			if (e.getMessage().startsWith("This port appears to have been shutdown or disconnected."))
			{
				checkRequest = true;
			}
			System.err.println(e.toString());
		}
	}

	public void setStopRequest()
	{

//		if (Main.getConfig().loading.settings.checksystem_modeID    == 2
//			|| Main.getConfig().loading.settings.io_startDelay  + 200
//				< Util.getTimeDifference(Main.m.checkSystem.startedAt))
		{
			stopRequest = true;
		}
	}

	protected boolean assert1(final String s)
	{

		if (s == null)
		{
			System.out.println("����� �����������");
			return false;
		}
		if (s.isEmpty())
		{
			System.err.println("������ ������");
		}
//		if (s.length() != 2)
//		{
//			System.out.println("���������� ��������� ����� (������ ������) " + s.length());
//			return false;
//		}

		final SssstdpMessage[ ] messages = SssstdpMessage.getMessages(s);

		if (debug)
		{
			System.out.println("Messages " + Arrays.toString(messages));
		}
		final boolean matches = messages != null && messages.length != 0;

		if (!matches)
		{
			System.out.println("����� �� ���������");
		}
		else
		{
			if (debug)
			{
				System.out.println("����� ���������");
			}
		}
		return matches;
	}

	protected void config()
	{
		serialPort.setComPortParameters(19200, 8, SerialPort.TWO_STOP_BITS, SerialPort.NO_PARITY);
		serialPort.setRs485ModeParameters(true, true, 0, 0);
		serialPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
		serialPort
				.setComPortTimeouts(
						SerialPort.TIMEOUT_NONBLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING,
						0,
						SerialManager.CONFIG_SPAM_TIMEOUT);

	}

	protected int getStatus()
	{
		int status = 0;
		int index = 0;
		if (serialPort.getCTS())//5
		{
			status |= 0x1 << index++;
		}
		if (serialPort.getDCD())//4
		{
			status |= 0x1 << index++;
		}
		if (serialPort.getDSR())//3
		{
			status |= 0x1 << index++;
		}
		if (serialPort.getDTR())//2
		{
			status |= 0x1 << index++;
		}
		if (serialPort.getRI())//1
		{
			status |= 0x1 << index++;
		}
		if (serialPort.getRTS())//0
		{
			status |= 0x1 << index++;
		}
		return status;
	}

	protected void reopen(final com.fazecast.jSerialComm.SerialPort serialPort)
	{
//		serialPort.setBreak();
//		serialPort.setDTR();
//		serialPort.setRTS();
//		serialPort.setBaudRate(300);
//		sendSingleByte((byte) 0xFF);
//
//		try
//		{
//			Thread.sleep(500);
//		}
//		catch (final Exception e)
//		{
//			throw new RuntimeException(e);
//		}
//		serialPort.clearBreak();
//		serialPort.clearRTS();
//		serialPort.clearDTR();
//		try
//		{
//			Thread.sleep(500);
//		}
//		catch (final Exception e)
//		{
//			throw new RuntimeException(e);
//		}
//		sendSingleByte((byte) 0xFD);works
//		serialPort.disablePortConfiguration();
//		try
//		{
//			Thread.sleep(250);
//		}
//		catch (final Exception e)
//		{
//			throw new RuntimeException(e);
//		}
		if (!serialPort.closePort())
		{
			throw new IllegalStateException("���� �� �����������");
		}
//		serialPort.disablePortConfiguration();
		try
		{
			Thread.sleep((long) (200 + Math.random() * 1000));
//			if (Math.random() < 0.2)
//			{
//				Thread.sleep(4000);
//			}
		}
		catch (final Exception e)
		{
			throw new RuntimeException(e);
		}
		config();

		if (!serialPort.openPort() || !serialPort.isOpen())
		{
			throw new IllegalStateException("���� �� �����������");
		}
		config();
		try
		{
			Thread.sleep(200);
		}
		catch (final Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Do not use
	 * @return
	 */
	private boolean checkConnectionImpl()
	{
		System.out.println("�������� �����");
//		if (getStatus() != 31)
//		{
//			return false;
//		}
		try
		{

			{
				for (int i = 0; i < 8; i++)
				{
					final String s = tr(SerialManager.MSG_EMPTY, "init");

					if (s == null)
					{
						Thread.sleep(50);
						return false;
					}

					System.out.println("���� ����� ");
					final boolean matches = assert1(s);
					if (matches)
					{

						Thread.sleep(50);
						return true;
					}

					Thread.sleep(50);
				}
			}
			return false;
		}
		catch (final Exception e)
		{
			System.err.println(e.getMessage());
			return false;
		}
	}

	private void register()
	{
		// TODO Auto-generated method stub

	}

	private void sendStopSignal()
	{
		try
		{
			if (debug)
			{
				System.err.println("SENDING STOP SIGNAL");
			}
			Thread.sleep(system.getConfiguration().getLong("nn.stopDelay"));
			synchronized (lock)
			{
				tr(SerialManager.MSG_CMD_STOP, "stop");
				Thread.sleep(25);
				tr(SerialManager.MSG_CMD_STOP, "stop");
				Thread.sleep(25);
				tr(SerialManager.MSG_CMD_STOP, "stop");
				Thread.sleep(25);
				tr(SerialManager.MSG_CMD_STOP, "stop");
				if (fullInit)
				{
					checkRequest = true;
				}
			}
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return;
		}
	}

	private String tr(final int msg, final String cause)
	{

		//		if (1 + 1 == 2)
//		{
//			try
//			{
//				Thread.sleep(200);
//			}
//			catch (final Exception e)
//			{
//				throw new RuntimeException(e);
//			}
//			System.out.println("Sending ");
//			final SssstdpMessage orNull = SssstdpMessage.getOrNull(msg);
//
//			final byte myByte = orNull.value();
//			final byte[ ] buffer = new byte[ ] { myByte, (byte) ~myByte };
//			final int writeBytes = serialPort.writeBytes(buffer, 2L);
//			System.out.println("Sent " + writeBytes + Arrays.toString(buffer));
////			serialPort.writeBytes(new byte[ ] { (byte) 0xFD }, 1);
////			serialPort.writeBytes(new byte[ ] { (byte) 0x01 }, 1);
//			final long timeout = 3;
//			final long t0 = System.currentTimeMillis();
//			final String read = listener.read(cause, timeout);
//			System.out.println(read);
////			System.out.println(listener.getMessage("asd"));
//			return read;
//		}
		final SssstdpMessage orNull = SssstdpMessage.getOrNull(msg);
		if (orNull == null)
		{
			throw new IllegalArgumentException("Tried to send msg with index " + msg);
		}
//		System.out.println("Msg " + orNull.toString());
		final byte myByte = orNull.value();
		final byte[ ] bufferTr = new byte[ ]
		{ myByte, (byte) ~myByte };
		try
		{
			synchronized (lock)
			{
				if (System.currentTimeMillis() - lastTr < SerialManager.CONFIG_SPAM_INTERVAL)
				{
					try
					{
						Thread
								.sleep(
										Math
												.min(
														SerialManager.CONFIG_SPAM_INTERVAL,
														System.currentTimeMillis() - lastTr
																+ SerialManager.CONFIG_SPAM_INTERVAL));
					}
					catch (final Exception e)
					{
						throw new RuntimeException(e);
					}
				}
				lastTr = System.currentTimeMillis();
				serialPort.getInputStream().skip(serialPort.bytesAvailable());
				final int writeBytes = serialPort.writeBytes(bufferTr, 2L);

//				System.out.println("Sent " + writeBytes + Arrays.toString(bufferTr));
//			serialPort.writeBytes(new byte[ ] { (byte) 0xFD }, 1);
//			serialPort.writeBytes(new byte[ ] { (byte) 0x01 }, 1);
				final long t0 = System.currentTimeMillis();
//			Thread.sleep(25);
				while (serialPort.bytesAvailable() < 2 && System.currentTimeMillis() - t0 < 60)
				{
					Thread.sleep(1);
				}
				final byte[ ] b__ = new byte[ serialPort.bytesAvailable() ];
				serialPort.readBytes(b__, b__.length);
				String read = new String(b__);
//			String read = listener.read("cause", 150);
				if (read == null)
				{
					read = "";
				}
				return read;
			}
		}
		catch (final Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	private void unregister()
	{
		// TODO Auto-generated method stub

	}
}
