package at.ji.wbchkr.configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;

import at.ji.wbchkr.configuration.file.ConfigurationIoNotSupportedException;

/**
 * This is a {@link Configuration} implementation that does not save or load
 * from any source, and stores all values in memory only.
 * This is useful for temporary Configurations for providing defaults.
 */
public class MemoryConfiguration extends MemorySection implements Configuration
{
	protected Configuration defaults;
	protected MemoryConfigurationOptions options;

	/**
	 * Creates an empty {@link MemoryConfiguration} with no default values.
	 */
	public MemoryConfiguration()
	{}

	/**
	 * Creates an empty {@link MemoryConfiguration} using the specified {@link
	 * Configuration} as a source for all default values.
	 *
	 * @param defaults Default value provider
	 * @throws IllegalArgumentException Thrown if defaults is null
	 */
	public MemoryConfiguration(final Configuration defaults)
	{
		this.defaults = defaults;
	}

	@Override
	public void addDefault(final String path, final Object value)
	{
		Validate.notNull(path, "Path may not be null");

		if (defaults == null)
		{
			defaults = new MemoryConfiguration();
		}

		defaults.set(path, value);
	}

	@Override
	public void addDefaults(final Configuration defaults)
	{
		Validate.notNull(defaults, "Defaults may not be null");

		addDefaults(defaults.getValues(true));
	}

	@Override
	public void addDefaults(final Map<String, Object> defaults)
	{
		Validate.notNull(defaults, "Defaults may not be null");

		for (final Map.Entry<String, Object> entry : defaults.entrySet())
		{
			addDefault(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public Configuration getDefaults()
	{
		return defaults;
	}

	@Override
	public ConfigurationSection getParent()
	{
		return null;
	}

	@Override
	public boolean isSaveLoadSupported()
	{
		return false;
	}

	@Override
	public void load()
		throws FileNotFoundException, IOException, InvalidConfigurationException, ConfigurationIoNotSupportedException
	{
		throw new ConfigurationIoNotSupportedException(defaults, false);
	}

	@Override
	public void loadFromString(final String contents) throws InvalidConfigurationException
	{
		throw new NotImplementedException("loadFromString(String)");
	}

	@Override
	public MemoryConfigurationOptions options()
	{
		if (options == null)
		{
			options = new MemoryConfigurationOptions(this);
		}

		return options;
	}

	@Override
	public void save()
		throws FileNotFoundException, IOException, InvalidConfigurationException, ConfigurationIoNotSupportedException
	{
		throw new ConfigurationIoNotSupportedException(defaults, true);
		
	}

	@Override
	public String saveToString()
	{
		throw new NotImplementedException("saveToString()");
	}

	@Override
	public void setDefaults(final Configuration defaults)
	{
		Validate.notNull(defaults, "Defaults may not be null");

		this.defaults = defaults;
	}
}
