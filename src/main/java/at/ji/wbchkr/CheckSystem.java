package at.ji.wbchkr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import org.webChecker.nd4j.debug.mvs.MVSCamera;

import at.ji.utils.massives.collections.DequeRevolved;
import at.ji.wbchkr.configuration.file.FileConfiguration;
import at.ji.wbchkr.configuration.file.YamlConfiguration;
import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class CheckSystem
{
	public static class SystemStatus
	{
		public volatile boolean isRewindAllowed = false;
		public volatile boolean led = false;
		public volatile String explanation = "������";
		public volatile String reason = "";
		public volatile long changed = System.currentTimeMillis();
	}
	
	/**
	 * @work ������
	 * @work_count ������ ������ �������� �����
	 * @skip ������� ����� �����
	 * @stop ���������
	 * @stop_check ��������� ��� ��������
	 * @stop_ext ��������� �������
	 * @sleep �������� ������ �������� �������, ������ ������ �������� �����
	 * @none ���������
	 */
	public enum WorkTask
	{
		WORK, WORK_COUNT, SLEEP, NONE;
		
		public boolean isCounterEnabled()
		{
			return equals(WORK) || equals(WORK_COUNT) || equals(SLEEP);
		}
	}
	
	private volatile boolean initialized = false;
	private final Object lock = new Object();
	
	private final YamlConfiguration configuration;
	private ImageProvider imageProvider;
	private EventListener listener;
	private final DequeRevolved<ImageRegistration> deque = new DequeRevolved<>(60 * 2);//TODO minibatchSize * 2
	private volatile SystemStatus status = new SystemStatus();
	private volatile long lastStopped = 0;
	private volatile WorkTask workTask = WorkTask.NONE;
	private SystemThread thread;
	private final SystemUpdater updater;
	private long globalThreadLastTick;
	private long systemThreadLastTick;
	
	public CheckSystem(final YamlConfiguration configuration)
	{
		this.configuration = configuration;
		workTask = (WorkTask.valueOf(getConfiguration().getString("status")));
		updater = new SystemUpdater(this);
	}
	
	public void deinitialize()
	{
		synchronized (lock)
		{
//			if (!isInitialized())
//			{
//				throw new IllegalStateException("������� ��������� �������������.");
//			}
			try
			{
				
				try
				{
					setImageProvider(new EmptyImageProvider());
				}
				catch (final Exception e)
				{
					System.err.println("�� ������� ������������ �������� ����������� �� " + getName());
					e.printStackTrace();
				}
				if (thread != null)
				{
					try
					{
						thread.interrupt();
					}
					catch (final Exception e)
					{
						e.printStackTrace();
					}
				}
				
				if (listener != null)
				{
					listener.event(this, "deinit", null, null);
				}
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
			try
			{
				getSessionManager().onStop();
			}
			catch (final Exception e)
			{
				e.printStackTrace();
			}
			initialized = false;
		}
	}
	
	public YamlConfiguration getConfiguration()
	{
		return configuration;
	}
	
	public DequeRevolved<ImageRegistration> getDeque()
	{
		return deque;
	}
	
	public long getGlobalThreadLastTick()
	{
		return globalThreadLastTick;
	}
	
	public ImageProvider getImageProvider()
	{
		synchronized (lock)
		{
			return imageProvider;
		}
	}
	
	public EventListener getListener()
	{
		return listener;
	}
	
	public String getName()
	{
		return getConfiguration().getString("name");
	}
	
	public SystemUpdater getSessionManager()
	{
		return getUpdater();
	}
	
	public SystemStatus getStatus()
	{
		return status;
	}
	
	public long getSystenThreadLastTick()
	{
		return systemThreadLastTick;
	}
	
	public String getTextForUi()
	{
		return getUpdater().getText();
	}
	
	public SystemThread getThread()
	{
		// TODO Auto-generated method stub
		return thread;
	}
	
	public SystemUpdater getUpdater()
	{
		return updater;
	}
	
	public WorkTask getWorkTask()
	{
		return workTask;
	}
	
	public void globalThreadTickNotify()
	{
		globalThreadLastTick = System.currentTimeMillis();
	}
	
	public boolean has()
	{
		synchronized (deque)
		{
			return !deque.isEmpty();
		}
	}
	
	public void initialize()
	{
		synchronized (lock)
		{
			if (isInitialized())
			{
				throw new IllegalStateException("������� ��������� �������������.");
			}
			try
			{
				
				try
				{
					setImageProvider(imgp());
				}
				catch (final Exception e)
				{
					throw new RuntimeException("�� ������� ������������ �������� ����������� �� " + getName(), e);
				}
				
				thread = new SystemThread(this);//TODO move
				thread.start();
				
				listener = createListenerImpl();
				
				if (listener != null)
				{
					listener.event(this, "init", null, null);
				}
			}
			catch (final Exception e)
			{
				throw new RuntimeException(e);
			}
			initialized = true;
		}
	}
	
	public boolean isInitialized()
	{
		return initialized;
	}
	
	public boolean isSleeping()
	{
		return getWorkTask().equals(WorkTask.SLEEP);
	}
	
	public ImageRegistration poll()
	{
		synchronized (deque)
		{
			return deque.poll();
		}
	}
	
	public void saveConfig()
	{
		try
		{
			final FileConfiguration fileConfiguration = (FileConfiguration) getConfiguration().getDefaults();
			fileConfiguration.setConfigurationFile(new File("systems/" + getName(), "config.yml"));
			fileConfiguration.save();
		}
		catch (final Exception e)
		{
			//another attempt
//			try
//			{
//				getConfiguration()
//						.setConfigurationFile(
//								new File(
//										getConfiguration().getConfigurationFile().getParentFile(),
//										"configBackup.yml"));
//				getConfiguration().getDefaults().save();
//			}
//			catch (final Exception e2)
//			{
//				e2.printStackTrace();
//			}
			throw new RuntimeException("���������� �� ����� ���������� �������", e);
		}
	}
	
	public void setImageProvider(final ImageProvider imageProvider) throws IOException
	{
		synchronized (lock)
		{
			imageProvider.set("fps", "" + configuration.getInt("nn.fps"));
			imageProvider.set("exposure", "" + configuration.getInt("image.exposition"));
			imageProvider.set("binning.horizontal", "" + configuration.getInt("image.binning.horizontal"));
//			imageProvider.set("binning.vertical", "" + configuration.getInt("image.binning.vertical")); TODO
			imageProvider.set("offset.vertical", "" + configuration.getInt("image.offset.vertical"));
			imageProvider.setEnabled();
			
			this.imageProvider = imageProvider;
		}
	}
	
	public void setListener(final EventListener listener)
	{
		synchronized (lock)
		{
			this.listener = listener;
		}
	}
	
	public void setWorkTask(final WorkTask task, final boolean allowUnmodified)
	{
		synchronized (this)
		{
			workTask = task;
			System.out.println("System " + getName() + " status is set to " + task);
		}
	}
	
	public void systemThreadTickNofity()
	{
		systemThreadLastTick = System.currentTimeMillis();
	}
	
	private EventListener createListenerImpl()
	{
		final String port = getConfiguration().getString("serial.commPort");
		
		if (port.equals("empty"))
		{
			return new EventListener()
			{
				
				@Override
				public void event(final CheckSystem system, final String id, final String data, final BufferedImage img)
				{}
				
				@Override
				public boolean isFine()
				{
					return true;
				}
			};
		}
		
		return new SystemListener(port, this);
	}
	
	private ImageProvider imgp() throws IOException
	{
		if (configuration.getString("image.protocol").equals("mvs"))
		{
			final String string = configuration.getString("image.id");
			final MVSCamera camera = MVSCamera.get(string);
			final MvsImageProvider res = new MvsImageProvider(camera);
			return res;
		}
		if (configuration.getString("image.protocol").equals("empty"))
		{
			return new EmptyImageProvider();
		}
		throw new NoSuchElementException("Image protocol (mvs, empty) " + configuration.getString("image.protocol"));
	}
}
