package at.ji.wbchkr.configuration.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import org.apache.commons.lang3.Validate;

import at.ji.wbchkr.configuration.Configuration;
import at.ji.wbchkr.configuration.InvalidConfigurationException;
import at.ji.wbchkr.configuration.MemoryConfiguration;

/**
 * This is a base class for all File based implementations of {@link
 * Configuration}
 */
public abstract class FileConfiguration extends MemoryConfiguration
{
	private File configurationFile;

	/**
	 * Creates an empty {@link FileConfiguration} with no default values.
	 */
	public FileConfiguration()
	{
		this(null);
	}

	/**
	 * Creates an empty {@link FileConfiguration} using the specified {@link
	 * Configuration} as a source for all default values.
	 *
	 * @param defaults Default value provider
	 * @param file
	 */
	public FileConfiguration(final Configuration defaults, final File file)
	{
		super(defaults);
		configurationFile = file;
	}
	
	/**
	 * Creates an empty {@link FileConfiguration} with no default values.
	 * @param file
	 */
	public FileConfiguration(final File file)
	{
		this(null, file);
	}
	
	public File getConfigurationFile()
	{
		return configurationFile;
	}

	@Override
	public boolean isSaveLoadSupported()
	{
		return configurationFile != null;
	}

	@Override
	public void load()
		throws NoConfigurationFileException, FileNotFoundException, IOException, InvalidConfigurationException
	{
		if (!isSaveLoadSupported())
		{
			throw new NoConfigurationFileException(this, false);
		}
		load(configurationFile);
	}

	/**
	 * Loads this {@link FileConfiguration} from the specified location.
	 * <p>
	 * All the values contained within this configuration will be removed,
	 * leaving only settings and defaults, and the new values will be loaded
	 * from the given file.
	 * <p>
	 * If the file cannot be loaded for any reason, an exception will be
	 * thrown.
	 *
	 * @param file File to load from.
	 * @throws FileNotFoundException Thrown when the given file cannot be
	 *     opened.
	 * @throws IOException Thrown when the given file cannot be read.
	 * @throws InvalidConfigurationException Thrown when the given file is not
	 *     a valid Configuration.
	 * @throws IllegalArgumentException Thrown when file is null.
	 */
	public void load(final File file) throws FileNotFoundException, IOException, InvalidConfigurationException
	{
		Validate.notNull(file, "File cannot be null");

		final FileInputStream stream = new FileInputStream(file);

//		load(new InputStreamReader(stream, Charsets.UTF_8));"
		load(new InputStreamReader(stream, "UTF-8"));

	}

	/**
	 * Loads this {@link FileConfiguration} from the specified reader.
	 * <p>
	 * All the values contained within this configuration will be removed,
	 * leaving only settings and defaults, and the new values will be loaded
	 * from the given stream.
	 *
	 * @param reader the reader to load from
	 * @throws IOException thrown when underlying reader throws an IOException
	 * @throws InvalidConfigurationException thrown when the reader does not
	 *      represent a valid Configuration
	 * @throws IllegalArgumentException thrown when reader is null
	 */
	public void load(final Reader reader) throws IOException, InvalidConfigurationException
	{
		final BufferedReader input = reader instanceof BufferedReader ? (BufferedReader) reader
			: new BufferedReader(reader);

		final StringBuilder builder = new StringBuilder();

		try
		{
			String line;

			while ((line = input.readLine()) != null)
			{
				builder.append(line);
				builder.append('\n');
			}
		}
		finally
		{
			input.close();
		}

		loadFromString(builder.toString());
	}

	/**
	 * Loads this {@link FileConfiguration} from the specified location.
	 * <p>
	 * All the values contained within this configuration will be removed,
	 * leaving only settings and defaults, and the new values will be loaded
	 * from the given file.
	 * <p>
	 * If the file cannot be loaded for any reason, an exception will be
	 * thrown.
	 *
	 * @param file File to load from.
	 * @throws FileNotFoundException Thrown when the given file cannot be
	 *     opened.
	 * @throws IOException Thrown when the given file cannot be read.
	 * @throws InvalidConfigurationException Thrown when the given file is not
	 *     a valid Configuration.
	 * @throws IllegalArgumentException Thrown when file is null.
	 */
	public void load(final String file) throws FileNotFoundException, IOException, InvalidConfigurationException
	{
		Validate.notNull(file, "File cannot be null");

		load(new File(file));
	}

	/**
	 * Loads this {@link FileConfiguration} from the specified string, as
	 * opposed to from file.
	 * <p>
	 * All the values contained within this configuration will be removed,
	 * leaving only settings and defaults, and the new values will be loaded
	 * from the given string.
	 * <p>
	 * If the string is invalid in any way, an exception will be thrown.
	 *
	 * @param contents Contents of a Configuration to load.
	 * @throws InvalidConfigurationException Thrown if the specified string is
	 *     invalid.
	 * @throws IllegalArgumentException Thrown if contents is null.
	 */
	@Override
	public abstract void loadFromString(String contents) throws InvalidConfigurationException;

	@Override
	public FileConfigurationOptions options()
	{
		if (options == null)
		{
			options = new FileConfigurationOptions(this);
		}

		return (FileConfigurationOptions) options;
	}

	@Override
	public void save() throws NoConfigurationFileException, IOException
	{
		if (!isSaveLoadSupported())
		{
			throw new NoConfigurationFileException(this, true);
		}
		save(configurationFile);
	}

	/**
	 * Saves this {@link FileConfiguration} to the specified location.
	 * <p>
	 * If the file does not exist, it will be created. If already exists, it
	 * will be overwritten. If it cannot be overwritten or created, an
	 * exception will be thrown.
	 * <p>
	 * This method will save using the system default encoding, or possibly
	 * using UTF8.
	 *
	 * @param file File to save to.
	 * @throws IOException Thrown when the given file cannot be written to for
	 *     any reason.
	 * @throws IllegalArgumentException Thrown when file is null.
	 */
	public void save(final File file) throws IOException
	{
		Validate.notNull(file, "File cannot be null");

//		Files.createParentDirs(file);
		if (!file.getParentFile().isDirectory())
		{
			try
			{
				if (!file.getParentFile().mkdirs())
				{
					throw new IOException("Failed to create folder " + file.getParentFile().getAbsolutePath());
				}
			}
			catch (final IOException e)
			{
				synchronized (System.err)
				{
					System.err.println("Plain exception");
					e.printStackTrace();
				}
			}
		}

		final String data = saveToString();

//		final Writer writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8);
		if (!file.exists())
		{
			if (!file.createNewFile())
			{
				throw new IOException("Failed to create file " + file.getAbsolutePath());
			}
		}
		final Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");

		try
		{
			writer.write(data);
		}
		finally
		{
			writer.close();
		}
	}

	/**
	 * Saves this {@link FileConfiguration} to the specified location.
	 * <p>
	 * If the file does not exist, it will be created. If already exists, it
	 * will be overwritten. If it cannot be overwritten or created, an
	 * exception will be thrown.
	 * <p>
	 * This method will save using the system default encoding, or possibly
	 * using UTF8.
	 *
	 * @param file File to save to.
	 * @throws IOException Thrown when the given file cannot be written to for
	 *     any reason.
	 * @throws IllegalArgumentException Thrown when file is null.
	 */
	public void save(final String file) throws IOException
	{
		Validate.notNull(file, "File cannot be null");

		save(new File(file));
	}

	public void setConfigurationFile(final File configurationFile)
	{
		this.configurationFile = configurationFile;
	}


	/**
	 * Compiles the header for this {@link FileConfiguration} and returns the
	 * result.
	 * <p>
	 * This will use the header from {@link #options()} -&gt; {@link
	 * FileConfigurationOptions#header()}, respecting the rules of {@link
	 * FileConfigurationOptions#copyHeader()} if set.
	 *
	 * @return Compiled header
	 */
	protected abstract String buildHeader();
}