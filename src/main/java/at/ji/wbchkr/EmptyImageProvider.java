package at.ji.wbchkr;

import java.awt.image.BufferedImage;
import java.io.IOException;

import at.ji.wbchkr.nn.Analizer.ImageRegistration;

public class EmptyImageProvider extends ImageProvider
{
	@Override
	public ImageRegistration get(final long timeout_ms) throws IOException
	{
		return new ImageRegistration(new BufferedImage(640, 360, 5), new byte[ 640 * 360 ], System.currentTimeMillis());
	}

	@Override
	public boolean isEnabled() throws IOException
	{
		return true;
	}

	@Override
	public void set(final String id, final String value) throws IOException
	{
	}

	@Override
	public void setEnabled() throws IOException
	{
	}
}