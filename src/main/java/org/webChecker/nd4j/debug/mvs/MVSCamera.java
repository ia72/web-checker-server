package org.webChecker.nd4j.debug.mvs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;

import javax.imageio.ImageIO;

import MvCameraControlWrapper.CameraControlException;
import MvCameraControlWrapper.CameraImageCallBack;
import MvCameraControlWrapper.MvCameraControl;
import MvCameraControlWrapper.MvCameraControlDefines;
import MvCameraControlWrapper.MvCameraControlDefines.Handle;
import MvCameraControlWrapper.MvCameraControlDefines.MVCC_ENUMVALUE;
import MvCameraControlWrapper.MvCameraControlDefines.MVCC_INTVALUE;
import MvCameraControlWrapper.MvCameraControlDefines.MV_CC_DEVICE_INFO;
import MvCameraControlWrapper.MvCameraControlDefines.MV_FRAME_OUT_INFO;
import at.ji.utils.graphics.UtilGraphics2D;

public class MVSCamera
{
	private static final int OFFSET_Y = 0x98;
	private static final int HEIGHT = 0x2d0;
	private static final int PIXEL_FORMAT = 17301505;
	private static final int EXPOSURE = 450;
	private boolean parallelMode = false;
	private Handle handle = null;
	private final MV_CC_DEVICE_INFO deviceInfo;
	private Dimension resolution = new Dimension(1280, 1024);
	private final AtomicReference<BufferedImage> imageReference = new AtomicReference<>();
	private Thread thread;
	private byte[ ] lastBytes;
	private int exposureTime = -1;
	private float fps = -1;
	private int binningHorizontal = 1;
	private int offsetVertical = 0;

	public MVSCamera(final MV_CC_DEVICE_INFO deviceInfo)
	{
		super();
		this.deviceInfo = deviceInfo;
	}

	public void close() throws CameraControlException
	{

		// Stop grabbing
		int nRet = MvCameraControl.MV_CC_StopGrabbing(handle);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("StopGrabbing failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
		nRet = MvCameraControl.MV_CC_CloseDevice(handle);
		if (MvCameraControlWrapper.MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Closing camera failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		} // Destroy handle
		nRet = MvCameraControl.MV_CC_DestroyHandle(handle);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("DestroyHandle failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
		handle = null;

		if (thread != null)
		{
			thread.interrupt();
		}
	}

	public MV_CC_DEVICE_INFO getDeviceInfo()
	{
		return deviceInfo;
	}

	public int getExposureTime()
	{
		return exposureTime;
	}

	public float getFps()
	{
		return fps;
	}

	public Handle getHandle()
	{
		return handle;
	}

	public BufferedImage getImage() throws IOException
	{
		if (!isEnabled())
		{
			final CameraControlException e = new CameraControlException();
			e.errMsg = "Camera is closed";
			throw new IOException(e);
		}
		if (parallelMode)
		{
			synchronized (imageReference)
			{

				if (imageReference.get() == null)
				{
					final CameraControlException e = new CameraControlException();
					e.errMsg = "No image. Wait for initialization";
					throw new IOException(e);
				}
				return imageReference.get();
			}
		}
		else
		{
			return readImageImpl();
		}
	}

	public byte[ ] getLastBytes()
	{
		return lastBytes;
	}

	public int getOffsetVertical()
	{
		return offsetVertical;
	}

	public Dimension getResolution()
	{
		return resolution;
	}

	public String getSerialNumber()
	{
		return deviceInfo.gigEInfo.serialNumber;
	}

	public boolean isEnabled()
	{
		return handle != null;
	}

	public boolean isParallelMode()
	{
		return parallelMode;
	}

	public void open() throws CameraControlException, IOException
	{
		final Dimension dim = resolution;

		createImpl();

		openImpl();
		setBinningImpl();

		// Register image callback
//		registerImpl();
		setSettingsDebugImpl();
		// set continuous acquisition
		setContImpl();

		startGrabbingImpl();
		if (parallelMode)
		{
			thread = new Thread(new Runnable()
			{

				@Override
				public void run()
				{
					while (isEnabled())
					{
						try
						{
							final BufferedImage image = readImageImpl();
							synchronized (imageReference)
							{
								imageReference.set(image);
							}
						}
						catch (final Exception e)
						{
							try
							{
								close();
							}
							catch (final CameraControlException e1)
							{
								e1.printStackTrace();
							}
							throw new RuntimeException(e);
						}
					}
				}
			});
			thread.start();
		}
	}

	public void setBinningHorizontal(final int set) throws IOException
	{
		binningHorizontal = set;
	}

	public void setExposureTime(final int exposureTime)
	{
		this.exposureTime = exposureTime;
	}

	public void setFps(final float fps)
	{
		this.fps = fps;
	}

	public void setHandle(final Handle hCamera)
	{
		handle = hCamera;
	}

	public void setHeight(final int h) throws IOException
	{
		int nRet;
		if (h != -1)
		{
			nRet = MvCameraControl.MV_CC_SetIntValue(handle, "Height", h / binningHorizontal);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
				throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
			}
		}
	}

	public void setOffsetVertical(final int offsetVertical)
	{
		this.offsetVertical = offsetVertical;
	}

	public void setOffsetY(final int offsetY) throws IOException
	{
		int nRet;
		if (offsetY != -1)
		{
			nRet = MvCameraControl.MV_CC_SetIntValue(handle, "OffsetY", offsetY);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
				throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
			}
		}
	}

	public void setResolution(final Dimension resolution)
	{
		this.resolution = resolution;
	}

	void setParallelMode(final boolean set) throws IOException
	{
		if (isEnabled())
		{
			throw new IOException("Camera is already initialized");
		}
		parallelMode = set;
	}

	private void createImpl() throws CameraControlException
	{
		String serialNumber = "???";
		try
		{
			serialNumber = getSerialNumber();
		}
		catch (final Exception e1)
		{
		}
		try
		{
			if (handle != null)
			{
				throw new IllegalStateException("Camera is already opened " + serialNumber);
			}
			handle = MvCameraControl.MV_CC_CreateHandle(deviceInfo);
		}
		catch (final CameraControlException e)
		{
			throw new RuntimeException("Failed to create handle for camera " + serialNumber + " " + deviceInfo, e);
		}
	}

	private void openImpl() throws CameraControlException
	{
		final int nRet = MvCameraControl.MV_CC_OpenDevice(handle);
		if (MvCameraControlWrapper.MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Connect to camera failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
		System.out.println("Camera opened" + getSerialNumber());
	}

	private BufferedImage readImageImpl() throws IOException
	{
		final long t0 = System.currentTimeMillis();
		// Get payload size
		final MVCC_INTVALUE stParam = new MVCC_INTVALUE();
		int nRet = MvCameraControl.MV_CC_GetIntValue(handle, "PayloadSize", stParam);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Get PayloadSize fail, errcode: [%#x]\n", nRet);
			throw new IOException("Get PayloadSize fail, errcode: [%#x]\n " + nRet);
		}
		// Get one frame
		final MV_FRAME_OUT_INFO stImageInfo = new MV_FRAME_OUT_INFO();
		final byte[ ] pData = new byte[ (int) stParam.curValue ];
		lastBytes = pData;
		nRet = MvCameraControl.MV_CC_GetOneFrameTimeout(handle, pData, stImageInfo, 1000);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("GetOneFrameTimeout fail, errcode:[%#x]\n", nRet);
			throw new IOException("GetOneFrameTimeout fail, errcode:[%#x]\n " + nRet);
		}

//		MVSCamera.printFrameInfo(stImageInfo);
		final byte[ ] bytes = pData;
		final int[ ] arr = new int[ bytes.length ];
		for (int i = 0; i < bytes.length; i++)
		{
			final int b = bytes[ i ] & 0xFF;
			arr[ i ] = b | b << 8 | b << 16 | 0xFF000000;
			final Color c = new Color(arr[ i ]);
			if (c.getGreen() != c.getRed() || c.getGreen() != c.getBlue())
			{
				throw new IllegalArgumentException();
			}
		}
		final BufferedImage img = UtilGraphics2D.toBufferedImage(arr, stImageInfo.width, stImageInfo.height);
		return img;
	}

	private void registerImpl() throws CameraControlException
	{
		int nRet;
		nRet = MvCameraControl.MV_CC_RegisterImageCallBack(handle, new CameraImageCallBack()
		{
			@Override
			public int OnImageCallBack(final byte[ ] bytes, final MV_FRAME_OUT_INFO mv_frame_out_info)
			{
				final int[ ] buffer = new int[ bytes.length ];
				MVSCamera.printFrameInfo(mv_frame_out_info);
				int summ = 0;
				for (int i = 0; i < bytes.length; i++)
				{
					summ += bytes[ i ] & 0xFF;
				}
				System.out.println(summ);
				return 0;
			}
		});
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("register image callback failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
	}

	private void setBinningImpl() throws IOException
	{
		int set = binningHorizontal;
		if (binningHorizontal == 1)
		{
			set = 1;
		}
		else if (binningHorizontal == 2)
		{
			set = 2;
		}
		else if (binningHorizontal == 4)
		{
			set = 3;
		}
		int nRet;

		final MVCC_ENUMVALUE ret = new MVCC_ENUMVALUE();
		MvCameraControl.MV_CC_GetEnumValue(handle, "BinningVertical", ret);
		System.out
				.println(
						"Camera " + getSerialNumber() + " " + "binning vertical" + " " + ret.curValue + " setting to "
								+ set);
		System.out.println(Arrays.toString(ret.supportValue.toArray(new Integer[ ret.supportValue.size() ])));
		nRet = MvCameraControl.MV_CC_SetEnumValue(handle, "BinningVertical", set);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
			throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
		}

		nRet = MvCameraControl.MV_CC_SetEnumValue(handle, "BinningHorizontal", set);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
			throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
		}
	}

	private void setContImpl() throws CameraControlException
	{
		int nRet;
		nRet = MvCameraControl.MV_CC_SetEnumValueByString(handle, "AcquisitionMode", "Continuous");
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Set AcquisitionMode to Continous failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}

		// Turn off trigger mode and stop acquisition
		nRet = MvCameraControl.MV_CC_SetEnumValueByString(handle, "TriggerMode", "Off");
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("SetTriggerMode failed, errcode: [%#x]\n", nRet);
		}
	}

	private void setSettingsDebugImpl() throws IOException
	{
		int nRet;
		if (exposureTime != -1)
		{
//			MVCC_INTVALUE mvcc_INTVALUE = new MVCC_INTVALUE();
//			MvCameraControl.MV_CC_GetIntValue(handle, "ExposureTime", mvcc_INTVALUE);
//			if(mvcc_INTVALUE.curValue
			nRet = MvCameraControl.MV_CC_SetFloatValue(handle, "ExposureTime", exposureTime);

			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
				throw new IOException(
						"Setting exposure time failed, errcode: [%#x]\n " + nRet + " exposure " + exposureTime);
			}
		}
		if (0 <= fps)
		{
//			MVCC_INTVALUE mvcc_INTVALUE = new MVCC_INTVALUE();
//			MvCameraControl.MV_CC_GetIntValue(handle, "ExposureTime", mvcc_INTVALUE);
//			if(mvcc_INTVALUE.curValue
			nRet = MvCameraControl.MV_CC_SetFloatValue(handle, "AcquisitionFrameRate", fps);

			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
				throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
			}
		}
		if (MVSCamera.PIXEL_FORMAT != -1)
		{
			nRet = MvCameraControl.MV_CC_SetEnumValue(handle, "PixelFormat", MVSCamera.PIXEL_FORMAT);
			if (MvCameraControlDefines.MV_OK != nRet)
			{
				System.err.printf("Setting settings failed, errcode: [%#x]\n", nRet);
				throw new IOException("Setting settings failed, errcode: [%#x]\n " + nRet);
			}
		}
		final int h = MVSCamera.HEIGHT;
		setHeight(h);
		final int offsetY = offsetVertical;
		setOffsetY(offsetY);
	}

	private void setSingleFrameImpl() throws CameraControlException
	{
		int nRet;
		nRet = MvCameraControl.MV_CC_SetEnumValueByString(handle, "AcquisitionMode", "SingleFrame");
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("Set AcquisitionMode to Continous failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
	}

	private void startGrabbingImpl() throws CameraControlException
	{
		int nRet;
		nRet = MvCameraControl.MV_CC_StartGrabbing(handle);
		if (MvCameraControlDefines.MV_OK != nRet)
		{
			System.err.printf("StartGrabbing failed, errcode: [%#x]\n", nRet);
			throw new CameraControlException();
		}
	}

	public static MVSCamera get(final String string) throws IOException
	{
		try
		{
			final ArrayList<MV_CC_DEVICE_INFO> list = MVSCamera.getList();
			for (final MV_CC_DEVICE_INFO info : list)
			{
				if (info.gigEInfo.serialNumber.equals(string))
				{
					return new MVSCamera(info);
				}
			}
		}
		catch (final Exception e)
		{
			throw new IOException(e);
		}
		throw new NoSuchElementException("�� ������� MVS ������ " + string);
	}

	public static ArrayList<MV_CC_DEVICE_INFO> getList() throws CameraControlException
	{
		return MvCameraControl
				.MV_CC_EnumDevices(MvCameraControlDefines.MV_GIGE_DEVICE | MvCameraControlDefines.MV_USB_DEVICE);
	}

	public static void main(final String[ ] args) throws CameraControlException, IOException
	{
		final MVSCamera mvsCamera = new MVSCamera(MVSCamera.getList().get(0));
		mvsCamera.open();
		while (true)
		{
			final BufferedImage img = mvsCamera.getImage();
			ImageIO.write(img, "png", new File("test.png"));
		}
//		mvsCamera.close();
	}

	private static void printFrameInfo(final MV_FRAME_OUT_INFO stFrameInfo)
	{
		if (null == stFrameInfo)
		{
			System.err.println("stFrameInfo is null");
			return;
		}

		final StringBuilder frameInfo = new StringBuilder("");
		frameInfo.append(("\tFrameNum[" + stFrameInfo.frameNum + "]"));
		frameInfo.append("\tWidth[" + stFrameInfo.width + "]");
		frameInfo.append("\tHeight[" + stFrameInfo.height + "]");
		frameInfo.append(String.format("\tPixelType[%#x]", stFrameInfo.pixelType.getnValue()));

		System.out.println(frameInfo.toString());
	}
}
